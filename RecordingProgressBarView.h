//
//  RecordingProgressBarView.h
//
//  Created by Hugo Hernandez on 6/13/16
//  Copyright (c) . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordingProgressBarView : UIView

@property BOOL tickOn;
@property BOOL normal;

-(void)redraw;
-(void)setMaximumTime:(int)seconds;
-(void)settickOn:(BOOL)_tickOn;
-(void)addShot;
-(void)finishShot;
-(void)updateShot:(double)seconds;
-(void)highightLastShotForRemoval:(BOOL)highlight;
-(void)removeLastShot;
-(void)setIsRecording:(BOOL)_isRecording;
-(void)clear;

@property CGContextRef context;
@property (retain) NSMutableArray *shots;

@end
