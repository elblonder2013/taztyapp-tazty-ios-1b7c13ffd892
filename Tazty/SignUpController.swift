//
//  SignUpController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignUpController: UIViewController {
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    var action: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToPrevious(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_showInfo")
        {
            if(action == "Terms")
            {
                let vc = segue.destination as? InfoGenericController
                vc?.textToTitle = "Terms of Service"
                vc?.urlToShow = "http://taztyapp.com/phone/terms-of-service.html"
            }
            else if(action == "Privacy")
            {
                let vc = segue.destination as? InfoGenericController
                vc?.textToTitle = "Privacy Policy"
                vc?.urlToShow = "http://taztyapp.com/phone/privacy-policy.html"
            }
        }
        else if(segue.identifier == "segue_registerSuccess")
        {
            let vc = segue.destination as? HomeController
            vc?.isAnonymous = false
        }
    }


    @IBAction func showTerms(_ sender: UIButton) {
        self.action = "Terms"
        self.performSegue(withIdentifier: "segue_showInfo", sender: sender)
    }
    
    @IBAction func showPrivacy(_ sender: UIButton) {
        self.action = "Privacy"
        self.performSegue(withIdentifier: "segue_showInfo", sender: sender)

    }
    
    @IBAction func btnRegisterUser(_ sender: UIButton) {
        
        if(validateInformation())
        {
            let newPost : [String:AnyObject] = ["email" : txtEmail.text! as AnyObject, "password" : txtPassword.text! as AnyObject, "username" : txtUsername.text! as AnyObject]
            Alamofire.request(ServicesRouter.signupEmail(parameter: newPost))
                .responseJSON{ response in
                    switch response.result{
                    case .success( _):
                        if((response.result.value) != nil)
                        {
                           let login = Login.init(json: JSON(response.result.value!))
                            if login.code == "200"
                            {
                            Session.sharedInstance.setUserID(login.userId!)
                            Session.sharedInstance.setPhoto(login.photo!)
                            Session.sharedInstance.setUsername(login.fullname!)
                            Session.sharedInstance.setLoginType("3")
                            
                            self.performSegue(withIdentifier: "segue_registerSuccess", sender: sender)
                            }
                            else{
                                DispatchQueue.main.async(execute: { 
                                     CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: login.message, titleForButton: "OK",  controller: self)
                                })
                               
                            }
                        }
                        break
                    case .failure(let error):
                        DispatchQueue.main.async(execute: { 
                            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We have problems with connection try later", titleForButton: "OK",  controller: self)
                        })
                        
                        print(error)
                        break
                    }
            }
        }
    }
    
    func validateInformation() -> Bool
    {
        if(!(self.txtEmail.text?.isEmpty)! && !(self.txtPassword.text?.isEmpty)! && !(self.txtUsername.text?.isEmpty)!)
        {
            if(CommonFunctions.validateEmail(self.txtEmail.text!))
            {
                return true
            }
            else
            {
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "E-mail address invalid", titleForButton: "OK",  controller: self)
            }
        }
        else{
            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Please fill out all fields.", titleForButton: "OK",  controller: self)
        }
        
        return false
    }
 
}
