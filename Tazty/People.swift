//
//  People.swift
//
//  Created by Victor Hernandez on 08/07/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

open class People: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kPeopleFullnameKey: String = "fullname"
	internal let kPeoplePhotoKey: String = "photo"
	internal let kPeopleUserIdKey: String = "user_id"
	internal let kPeopleIsFollowingKey: String = "is_following"
	internal let kPeopleEmailKey: String = "email"


    // MARK: Properties
	open var fullname: String?
	open var photo: String?
	open var userId: String?
	open var isFollowing: String?
	open var email: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		fullname = json[kPeopleFullnameKey].string
		photo = json[kPeoplePhotoKey].string
		userId = json[kPeopleUserIdKey].string
		isFollowing = json[kPeopleIsFollowingKey].string
		email = json[kPeopleEmailKey].string

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    open func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if fullname != nil {
			dictionary.updateValue(fullname! as AnyObject, forKey: kPeopleFullnameKey)
		}
		if photo != nil {
			dictionary.updateValue(photo! as AnyObject, forKey: kPeoplePhotoKey)
		}
		if userId != nil {
			dictionary.updateValue(userId! as AnyObject, forKey: kPeopleUserIdKey)
		}
		if isFollowing != nil {
			dictionary.updateValue(isFollowing! as AnyObject, forKey: kPeopleIsFollowingKey)
		}
		if email != nil {
			dictionary.updateValue(email! as AnyObject, forKey: kPeopleEmailKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.fullname = aDecoder.decodeObject(forKey: kPeopleFullnameKey) as? String
		self.photo = aDecoder.decodeObject(forKey: kPeoplePhotoKey) as? String
		self.userId = aDecoder.decodeObject(forKey: kPeopleUserIdKey) as? String
		self.isFollowing = aDecoder.decodeObject(forKey: kPeopleIsFollowingKey) as? String
		self.email = aDecoder.decodeObject(forKey: kPeopleEmailKey) as? String

    }

    open func encode(with aCoder: NSCoder) {
		aCoder.encode(fullname, forKey: kPeopleFullnameKey)
		aCoder.encode(photo, forKey: kPeoplePhotoKey)
		aCoder.encode(userId, forKey: kPeopleUserIdKey)
		aCoder.encode(isFollowing, forKey: kPeopleIsFollowingKey)
		aCoder.encode(email, forKey: kPeopleEmailKey)

    }

}
