//
//  ProfileController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import MapKit
import AFNetworking
import SwiftyJSON
import QuartzCore
import SwiftyGif

class ProfileController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgEdit: UIImageView!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var btnEdit: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPostsNumber: UILabel!
    @IBOutlet weak var vwNoPost: UIView!
    @IBOutlet weak var tblFeed: UITableView!
    @IBOutlet weak var vwLoading: UIView!
    @IBOutlet weak var imgLoadingGif: UIImageView!
    @IBOutlet weak var lblFollowersNumber: UILabel!
    @IBOutlet weak var lblFollowingsNumber: UILabel!
    @IBOutlet weak var imgFollows: UIImageView!
    
    var feed_user : String?
    var arrFeed = [Feed]()
    var isFirstLoad = true
    var FollowerOrFollowing = false
    var numberPage : Int = 0
    var isPageRefreshing = false
    var isFollowing = false
    
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        super.viewDidLayoutSubviews()
        self.configureViewForFeedUser()
        self.getProfile()
        
        self.imgUser.layer.cornerRadius = 30
        
        self.imgUser.clipsToBounds = true
        self.imgUser.layer.borderWidth = 1
        self.imgUser.layer.borderColor = UIColor.white.cgColor
        self.imgUser.layer.masksToBounds = true
        
        self.imgBack.isUserInteractionEnabled = true
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(ProfileController.goBack(_:)))
        self.imgBack.addGestureRecognizer(tap2)
        
        self.lblFollowersNumber.isUserInteractionEnabled = true
        let tapFollowers = UITapGestureRecognizer(target: self, action: #selector(ProfileController.openFollowers(_:)))
        self.lblFollowersNumber.addGestureRecognizer(tapFollowers)
        
        self.lblFollowingsNumber.isUserInteractionEnabled = true
        let tapFollowings = UITapGestureRecognizer(target: self, action: #selector(ProfileController.openFollowings(_:)))
        self.lblFollowingsNumber.addGestureRecognizer(tapFollowings)
        
        self.imagePicker.delegate = self
        self.tblFeed.dataSource = self
        self.tblFeed.delegate = self
        
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        let gif = UIImage.init(gifName: "load")
        self.imgLoadingGif.setGifImage(gif, manager: gifmanager, loopCount:-1)
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCellHome(_:)), name: NSNotification.Name(rawValue: "deleteCellHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCellHome(_:)), name: NSNotification.Name(rawValue: "updateCellHome"), object: nil)
        
        view.addGestureRecognizer(edgePan)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func goBack(_ gestureRecognizer: UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    func openFollowers(_ gestureRecognizer: UITapGestureRecognizer)
    {
        self.FollowerOrFollowing = false
        self.performSegue(withIdentifier: "segue_follow", sender: nil)
    }
    
    func openFollowings(_ gestureRecognizer: UITapGestureRecognizer)
    {
        self.FollowerOrFollowing = true
        self.performSegue(withIdentifier: "segue_follow", sender: nil)
    }
    
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func configureViewForFeedUser()
    {
        if(Session.sharedInstance.getUserID()! == feed_user)
        {
            self.btnEdit.isHidden = false
            self.imgSettings.isHidden = false
            self.imgFollows.isHidden = true
            self.configureActions()
        }
        else
        {
            self.btnEdit.isHidden = true
            self.imgSettings.isHidden = true
            self.imgFollows.isHidden = false
            
            self.imgFollows.isUserInteractionEnabled = true
            let tapFollows = UITapGestureRecognizer.init(target: self, action: #selector(ProfileController.follow(_:)))
            self.imgFollows.addGestureRecognizer(tapFollows)
        }
    }
    
    func getProfile(){
 
        let newPost = ["userId": Session.sharedInstance.getUserID()!, "feedUser":feed_user!, "page": "\(self.numberPage)", "rows": "10"]
        Alamofire.request(ServicesRouter.getProfileInformation(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        let jsonResult = JSON(response.result.value!)
                        
                        if jsonResult["result"].stringValue == "Error"
                        {
                            self.vwLoading.isHidden = true
                            self.vwNoPost.isHidden = false
                        }
                        
                        else{
                        
                        self.lblUserName.text = jsonResult["data"]["fullname"].stringValue
                        var urlImage = jsonResult["data"]["photo"].stringValue
                        if(!jsonResult["data"]["photo"].stringValue.contains("http"))
                        {
                            urlImage = "http://38.100.119.14/fballr/photos/" + urlImage
                        }
                        self.imgUser.image = UIImage(named:"profile_img_default")
                        self.imgUser.setImageWith(URL(string: urlImage)!)
                        
                        
                        self.lblPostsNumber.text = jsonResult["data"]["post_count"].string! + " posts";
                        self.lblFollowersNumber.text = jsonResult["data"]["follower_count"].string! + " followers";
                        self.lblFollowingsNumber.text = jsonResult["data"]["following_count"].string! + " following";
                        self.imgFollows.image = jsonResult["data"]["is_following"].string! == "0" ? UIImage(named: "mas") : UIImage(named: "palomita")
                            self.isFollowing = jsonResult["data"]["is_following"].string! == "0" ? false : true
                        var arrRes = [JSON]()
                        if let resData = jsonResult["data"]["posts"].array{
                            arrRes = resData
                        }
                        
                        if(arrRes.count > 0)
                        {
                            self.vwNoPost.isHidden = true
                            for itemFeed in arrRes{
                                let feedItem = Feed.init(json: itemFeed)
                                self.arrFeed.append(feedItem)
                                
                            }
                            self.tblFeed.reloadData()
                        }
                        else
                        {
                            
                            self.vwLoading.isHidden = true
                            if self.numberPage == 0
                            {
                            self.vwNoPost.isHidden = false
                            }
                        }
                        }
                        
                        self.isPageRefreshing = false
                    }
                case .failure(let error):
                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We can't retrieve information, try again later", titleForButton: "OK", controller: self)
                    print(error)
                }
        }
    }
    
    func configureActions()
    {
        self.imgSettings.isUserInteractionEnabled = true
        self.btnEdit.isUserInteractionEnabled = true
        self.imgUser.isUserInteractionEnabled = true
        self.lblUserName.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileController.goToSettings(_:)))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(ProfileController.editImage(_:)))
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(ProfileController.editImage(_:)))
        let tap5 = UITapGestureRecognizer(target: self, action: #selector(ProfileController.editFullname(_:)))
        
        self.imgSettings.addGestureRecognizer(tap)
        self.btnEdit.addGestureRecognizer(tap3)
        self.imgUser.addGestureRecognizer(tap4)
        self.lblUserName.addGestureRecognizer(tap5)
    }
    
    func loadImageFromPhotoLibrary() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func loadImageFromCamera() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func editFullname(_ gestureRecognizer: UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: "Tazty", message: "Please input new username.", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Username"
        })
        alert.textFields?.first?.keyboardType = UIKeyboardType.emailAddress
        
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            if(!(textField.text?.isEmpty)!)
            {
            let newPost = ["user_id" : Session.sharedInstance.getUserID()!, "fullname" : textField.text!]
            Alamofire.request(ServicesRouter.updateFullname(parameter: newPost)).responseJSON{ response in
                switch response.result{
                case .success( _):
                    let jsonResult = JSON(response.result.value!)
                  //  print(response.result.value)
                    Session.sharedInstance.setUsername(jsonResult["fullname"].string!)
                    self.lblUserName.text = jsonResult["fullname"].string!
                    break
                case .failure( _):
                     CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "An error ocurred while update your username try again later", titleForButton: "OK",  controller: self)
                    break
                }
            }
            }
            else{
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Username field can't be empty", titleForButton: "OK",  controller: self)
            
            }
        }))
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func editImage(_ gestureRecognizer: UITapGestureRecognizer){
        self.showActionSheet()
    }
    
    func goToSettings(_ gestureRecognizer: UITapGestureRecognizer){
        self.performSegue(withIdentifier: "segue_settings", sender: nil)
    }

    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let pickedImage : UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.imgUser.contentMode = .scaleAspectFill
            self.imgUser.image = pickedImage
            upload(multipartFormData: { (multipartFormdata) in
                if let imageData = UIImageJPEGRepresentation(pickedImage, 1)
                {
                    multipartFormdata.append(imageData, withName: "photo", fileName: ("profile" + Session.sharedInstance.getUserID()! + String.init(format:"_%@", String(Date().timeIntervalSince1970).replacingOccurrences(of: ".", with: "")) + ".jpg"), mimeType: "image/jpeg")
                    multipartFormdata.append(Session.sharedInstance.getUserID()!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName :"user_id")
                }
            }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: (Session.sharedInstance.getAPIURL()! + "/user/update_photo"), method: HTTPMethod.post, headers: ["T-APP-ID" : Session.sharedInstance.getAPPID()!]) { (encodingResult) in
                switch encodingResult{
                case .success(let upload, _, _):
                    upload.responseJSON(completionHandler: { response in
                        if response.result.isSuccess
                        {
                        let jsonResult = JSON(response.result.value!)
                        Session.sharedInstance.setPhoto(jsonResult["photo"].string!)
                        print(jsonResult["photo"])
                        DispatchQueue.main.async(execute: {
                            self.imgUser.setImageWith(URL(string: jsonResult["photo"].string!)!)
                            self.arrFeed.removeAll()
                            self.tblFeed.reloadData()
                            self.getProfile()
                        })
                        }
                        else
                        {
                            print(response.result.error ?? "")
                        }
                    })
                case .failure(let encodingError):
                    print(encodingError)
                }
            }
        
        dismiss(animated: true, completion: nil)
    }
    
    func updateCellHome(_ notification: Notification)
    {
        let feedInfo = (notification as NSNotification).userInfo!["cellInfo"] as! Feed
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if((indexPath as NSIndexPath).row < self.arrFeed.count)
        {
        self.arrFeed[(indexPath as NSIndexPath).row] = feedInfo
        
        self.tblFeed.reloadData()
        }
    }
    
    func deleteCellHome(_ notification : Notification)
    {
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if indexPath.row < self.arrFeed.count
        {
        self.arrFeed.remove(at: indexPath.row)
        
        self.tblFeed.beginUpdates()
        self.tblFeed.deleteRows(at: [indexPath], with: .fade)
        self.tblFeed.endUpdates()
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Action Sheet
    func showActionSheet() {
        let optionMenu = UIAlertController(title: nil, message: "Add Photo", preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.loadImageFromCamera()
        })
        let photoLibraryAction = UIAlertAction(title: "From Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.loadImageFromPhotoLibrary()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)

        optionMenu.addAction(cameraAction)
        optionMenu.addAction(photoLibraryAction)
        optionMenu.addAction(cancelAction)

        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 430
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FeedCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedCell
        
        let feed = self.arrFeed[(indexPath as NSIndexPath).row]
        
        
        let origin = CLLocationCoordinate2D.init(latitude: Double(feed.mylatitude!)!, longitude: Double(feed.mylongitude!)!)
        
        let userLocation = CLLocationCoordinate2D.init(latitude: Double(Session.sharedInstance.getLatitude()!)!, longitude: Double(Session.sharedInstance.getLongitude()!)!)
        
        let distance = String(format: "%.2f", CommonFunctions.distanceMiles(origin, destination: userLocation))
        
        cell.indexPath = indexPath
        cell.lblFoodName.text                   = feed.foodname
        cell.lblRestaurantName.text             = feed.barname! + " - " + distance + "mi"
        cell.lblLikeCounter.titleLabel?.text    = feed.likesNum
        cell.lblUser.text                       = feed.fullname
        cell.lblTimeElapsed.text                = CommonFunctions.timeElapsed(feed.postDate!)
        
        cell.lblLikeCounter.setTitle(feed.likesNum, for: UIControlState())
        cell.lblLikeCounter.setTitleColor(feed.likeState == "0" ? Session.sharedInstance.getOrangeTazty() : UIColor.white, for: UIControlState())
        cell.lblLikeCounter.backgroundColor     = feed.likeState == "0" ? UIColor.white : Session.sharedInstance.getOrangeTazty()

        cell.likeButton.setImage(feed.likeState == "0" ? UIImage(named:"Tastys_empty") : UIImage(named:"Tastys_Fill"), for: UIControlState())
        
        cell.item = feed
        cell.viewController = self
        
        //Imagen
        cell.imgUser.image = UIImage(named: "profile_img_default")
        let url = URL(string: feed.photo!)
        if url != nil
        {
            cell.imgUser.setImageWith(url!)
        }

        //Video
        let vURLString = "http://38.100.119.14/fballr/uploads/" + feed.video!
        cell.currentURL = vURLString
        
        VideoManager.downloadVideoWithURL(vURLString)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if (indexPath as NSIndexPath).row == (tableView.indexPathsForVisibleRows?.last as NSIndexPath?)?.row
        {
            if(self.isFirstLoad)
            {
               
              self.goTableTop()
            self.isFirstLoad = false
            }
        }
    }
    
    func follow(_ gestureRecognizer : UITapGestureRecognizer)
    {
        if self.isFollowing
        {
            let newPost : [String : AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "userToFollow" : self.feed_user! as AnyObject]
            Alamofire.request(ServicesRouter.unfollow(parameter: newPost))
                .responseJSON{ response in
                    switch response.result{
                    case .success( _):
                        self.imgFollows.image = UIImage(named:"mas")
                        self.isFollowing = false
                        self.arrFeed.removeAll()
                        self.getProfile()
                        
                        let postUser : [String : Any] = ["user": self.feed_user!, "isFollowing" : false]
                        UserDefaults.standard.set(postUser, forKey: "userUpdated")
                        UserDefaults.standard.synchronize()
                        
                        break
                    case .failure( _):
                        
                        break
                    }
            }
        }
        else
        {
            let newPost : [String : AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "userToFollow" : self.feed_user! as AnyObject]
            Alamofire.request(ServicesRouter.follow(parameter: newPost))
                .responseJSON{ response in
                    switch response.result{
                    case .success( _):
                        self.imgFollows.image = UIImage(named:"palomita")
                        self.isFollowing = true
                        self.arrFeed.removeAll()
                        self.getProfile()
                        
                        let postUser : [String : Any] = ["user": self.feed_user!, "isFollowing" : true]
                        UserDefaults.standard.set(postUser, forKey: "userUpdated")
                        UserDefaults.standard.synchronize()
                        
                        break
                    case .failure( _):
                        
                        break
                    }
            }
        }
    }

    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.arrFeed.count>=10
        {
        if self.tblFeed.contentOffset.y >= self.tblFeed.contentSize.height - self.tblFeed.bounds.size.height
        {
            if self.isPageRefreshing == false
            {
                self.isPageRefreshing = true
                self.numberPage += 10
                self.getProfile()
                self.tblFeed.reloadData()
            }
        }
        }
    }
    
    func goTableTop()
    {
        self.tblFeed.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
         self.vwLoading.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let feedCell : FeedCell = cell as! FeedCell
        feedCell.stopAndRemoveCurrentVideo()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_profile_restaurant"
        {
            if sender is Feed
            {
                let  itemFeed = sender as! Feed
                let vc = segue.destination as? RestaurantController
                vc?.countVideos = "videos"
                vc?.restaurantName = itemFeed.barname
                vc?.distance = "mi"
                
                let barlocation = CLLocationCoordinate2D.init(latitude: Double(itemFeed.mylatitude!)!, longitude: Double(itemFeed.mylongitude!)!)
                
                vc?.restaurantLocation = barlocation
            }
        }
        else if segue.identifier == "segue_follow"
        {
            let vc = segue.destination as? FollowsController
            vc?.feedUser = self.feed_user
            
            if self.FollowerOrFollowing
            {
                vc?.isFollowing = true
            }
            else
            {
                vc?.isFollower = true
            }
            
        }
        else if segue.identifier == "segue_profile_to_dish"
        {
            if sender is Feed
            {
            let index = sender as! Feed
            
            let vc = segue.destination as! DishController
            vc.foodname = index.foodname
            }
        }
    }
}
