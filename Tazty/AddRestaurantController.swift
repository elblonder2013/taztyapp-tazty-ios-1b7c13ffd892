//
//  AddRestaurantController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class AddRestaurantController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate,UITextFieldDelegate {

    var videoURL : URL?
    var restaurant  = Restaurant.init(json: nil)
    var thumb : UIImage?
    var locationManager : CLLocationManager!
    var isInitialized = false
    var userLocationMap : CLLocationCoordinate2D!
    
    @IBOutlet weak var mapview: MKMapView!
    @IBOutlet weak var txtRestaurant: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtState: UITextField!
    @IBOutlet weak var txtCountry: UITextField!
    @IBOutlet weak var vwData: UIView!
    @IBOutlet weak var bottomConstraingForTexfields: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        locationManager.startUpdatingLocation()
        
        self.mapview.delegate = self
        self.mapview.mapType = .standard
        self.mapview.showsUserLocation = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(AddRestaurantController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddRestaurantController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.txtRestaurant.frame.height))
        txtRestaurant.leftView = paddingView
        txtRestaurant.leftViewMode = UITextFieldViewMode.always
        
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.txtAddress.frame.height))
        txtAddress.leftView = paddingView2
        txtAddress.leftViewMode = UITextFieldViewMode.always
        
        let paddingView3 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.txtCity.frame.height))
        txtCity.leftView = paddingView3
        txtCity.leftViewMode = UITextFieldViewMode.always
        
        let paddingView4 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.txtState.frame.height))
        txtState.leftView = paddingView4
        txtState.leftViewMode = UITextFieldViewMode.always
        
        let paddingView5 = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.txtCountry.frame.height))
        txtCountry.leftView = paddingView5
        txtCountry.leftViewMode = UITextFieldViewMode.always

        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        view.addGestureRecognizer(edgePan)
        
        
        self.txtRestaurant.delegate = self
        self.txtAddress.delegate = self
        self.txtCity.delegate = self
        self.txtState.delegate = self
        self.txtCountry.delegate = self
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {

            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.bottomConstraingForTexfields.constant = keyboardSize.height
            })

        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {

            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.bottomConstraingForTexfields.constant = 0
            })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isInitialized {
            isInitialized = true
            
            let userLocation: CLLocation = locations[0]
            let latitude = userLocation.coordinate.latitude
            let longitude = userLocation.coordinate.longitude
            let latDelta: CLLocationDegrees = 0.2
            let lonDelta: CLLocationDegrees = 0.2
            let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
            let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            self.mapview.setRegion(region, animated: true)
            self.mapview.showsUserLocation = true
            self.userLocationMap = locations[0].coordinate
            
            locationManager.stopUpdatingLocation()
        }
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        if (txtRestaurant.text!.isEmpty
            || txtAddress.text!.isEmpty
            || txtCity.text!.isEmpty
            || txtState.text!.isEmpty
            || txtCountry.text!.isEmpty)
        {
            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "All fields must be completed", titleForButton: "OK", controller: self)
        }
        else
        {
            self.restaurant.barname = txtRestaurant.text!
            self.restaurant.latitude = Session.sharedInstance.getLatitude()
            self.restaurant.longitude = Session.sharedInstance.getLongitude()
            self.restaurant.videos = txtAddress.text! + " - " + txtCity.text! + " - " + txtState.text! + " - " + txtCountry.text!
            
            self.performSegue(withIdentifier: "segue_add_dish", sender: nil)
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_add_dish"
        {
            let vc = segue.destination as! DishPickerController
            vc.restaurant = self.restaurant
            vc.videoURL = self.videoURL
            vc.thumb = self.thumb
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        switch textField
        {
        case self.txtRestaurant:
            self.txtAddress.becomeFirstResponder()
            break
        case self.txtAddress:
            self.txtCity.becomeFirstResponder()
            break
        case self.txtCity:
            self.txtState.becomeFirstResponder()
            break
        case self.txtState:
            self.txtCountry.becomeFirstResponder()
            break
        case self.txtCountry:
            textField.resignFirstResponder()
            break
        default:
            textField.resignFirstResponder()
        }
        return true
    }
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

}
