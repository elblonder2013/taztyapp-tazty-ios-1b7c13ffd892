//
//  MapController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON

class MapController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    var locationManager : CLLocationManager!
    var isInitialized = false
    var selectedAnnotation : MKAnnotationView!
    var userLocationMap : CLLocationCoordinate2D!
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        locationManager.startUpdatingLocation()
        
        self.mapView.delegate = self
        self.mapView.mapType = .standard
        self.mapView.showsUserLocation = true
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !isInitialized {
            isInitialized = true
            
            let userLocation: CLLocation = locations[0]
            let latitude = userLocation.coordinate.latitude
            let longitude = userLocation.coordinate.longitude
            let latDelta: CLLocationDegrees = 0.2
            let lonDelta: CLLocationDegrees = 0.2
            let span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, lonDelta)
            let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            let region: MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            self.mapView.setRegion(region, animated: true)
            self.mapView.showsUserLocation = true
            self.userLocationMap = locations[0].coordinate
            self.getFeedToMap(String(format:"%f",locations[0].coordinate.latitude), longitude: String(format:"%f",locations[0].coordinate.longitude))
            locationManager.stopUpdatingLocation()
        }
    }

    
    func mapView(_ mapView: MKMapView,
                 viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKUserLocation) { return nil }
        
        let reuseID = "taztyBar"
        var v = mapView.dequeueReusableAnnotationView(withIdentifier: reuseID) as? MKPinAnnotationView
        
        if v != nil {
            v!.annotation = annotation
        } else {
            v = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseID)
            
            let detailButton = UIButton.init(type: .detailDisclosure)
            detailButton.addTarget(self, action: #selector(MapController.handlePinButton(_:)), for: .touchUpInside)
            
            v?.canShowCallout = true
            v?.calloutOffset = CGPoint(x: 0, y: 0)
            v?.rightCalloutAccessoryView = detailButton
            v?.tintColor = UIColor.init(red: 0.961, green: 0.427, blue: 0.267, alpha: 1)
            
                if #available(iOS 9.0, *) {
                    v?.pinTintColor = UIColor.init(red: 0.961, green: 0.427, blue: 0.267, alpha: 1)
                } else {
                    v?.pinColor = .red
                }
            
            
//            let pinImage = UIImage(named: "bar_location_map")
//            let size = CGSize(width: 40, height: 40)
//            UIGraphicsBeginImageContext(size)
//            pinImage!.drawInRect(CGRectMake(0, 0, size.width, size.height))
//            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
            
        }
        
        return v
    }
    
    func getFeedToMap(_ latitude: String, longitude: String)
    {
        var arrFeed = [FeedMap]()
        let newPost = ["latitude": latitude, "longitude": longitude]
        Alamofire.request(ServicesRouter.getFeedToMap(parameter: newPost))
            .responseJSON { response in
                
                switch response.result{
                case .success( _):
                    if ((response.result.value) != nil) {
                        let swiftyJsonVar = JSON(response.result.value!)
                        
                        
                            for item in swiftyJsonVar["data"].array!
                            {
                                let feed = FeedMap.init(json: item)
                                arrFeed.append(feed)
                            }
                            
                            for item in arrFeed{
                                let myAnnotation = MKPointAnnotation.init()
                                myAnnotation.coordinate = CLLocationCoordinate2DMake(Double.init(item.latitude!)!, Double.init(item.longitude!)!)
                                myAnnotation.title =  item.barname
                                myAnnotation.subtitle = String.init(format: "%d Videos", item.dishes!)
                                
                                self.mapView.addAnnotation(myAnnotation)
                            }
                            
                        
                        
                        
                    }
                case .failure(_):
                   CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We have problems with connection try later",  titleForButton: "OK", controller: self)
                }
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        self.selectedAnnotation = view
        
        var zoomRect = MKMapRectNull
        let annotationPoint = MKMapPointForCoordinate(view.annotation!.coordinate)
        let pointRect = MKMapRectMake(annotationPoint.x - 2500.0, annotationPoint.y - 2500.0, 5000.0, 5000.0)
        
        zoomRect = MKMapRectUnion(zoomRect, pointRect)
        self.mapView.setVisibleMapRect(zoomRect, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_restaurant")
        {
            let vc = segue.destination as? RestaurantController
            vc?.countVideos = self.selectedAnnotation.annotation?.subtitle!
            vc?.restaurantName = self.selectedAnnotation.annotation?.title!
            vc?.distance = String.init(format:"%0.2f",CommonFunctions.distanceMiles(self.selectedAnnotation.annotation!.coordinate, destination: self.userLocationMap))
            vc?.restaurantLocation = self.selectedAnnotation.annotation?.coordinate
        }
    }
    
    func handlePinButton(_ btn: UIButton){
        self.performSegue(withIdentifier: "segue_restaurant", sender: btn)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
