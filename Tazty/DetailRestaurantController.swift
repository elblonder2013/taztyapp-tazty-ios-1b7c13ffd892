//
//  DetailRestaurantController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/26/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class DetailRestaurantController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tblDetails: UITableView!
    var barname: String = ""
    var barLocation: CLLocationCoordinate2D!
    var arrFeed: [AnyObject] = []
    var barType: String = ""
    var barAddress: String = ""
    var barPhone: String = ""
    var barPhoneToCall: String = ""
    var barWebsite: String = ""
    var barHours: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblDetails.delegate = self;
        self.tblDetails.dataSource = self;
        self.requestRestaurantDetail()
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        self.tblDetails.layoutMargins = UIEdgeInsets.zero
        self.tblDetails.separatorInset = UIEdgeInsets.zero
        
        view.addGestureRecognizer(edgePan)
        
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func requestRestaurantDetail()
    {
        request("https://api.foursquare.com/v2/venues/search",
                method: .get,
                parameters: ["client_id": "5P1OVCFK0CCVCQ5GBBCWRFGUVNX5R4WGKHL2DGJGZ32FDFKT",
                             "client_secret" : "UPZJO0A0XL44IHCD1KQBMAYGCZ45Z03BORJZZJXELPWHPSAR",
                             "v" : "20130815",
                             "ll" : String.init(format:"%f,%f",self.barLocation.latitude, self.barLocation.longitude),
                             "query" : String.init(format:"%@",self.barname)],
                encoding: URLEncoding.default,
                headers: nil)
            .responseJSON { (response) in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        let dic = JSON(response.result.value!)
                        
                        let venues = dic["response"]["venues"].array
                        
                        let venue = venues?.first
                        let venue_location = venue?["location"]
                        self.barWebsite = venue?["url"] != JSON.null ? (venue?["url"].stringValue)! : ""
                        let categories = venue?["categories"] != JSON.null ? venue?["categories"].array : []
                        if categories!.count > 0
                        {
                            self.barType = (categories?.first?["pluralName"].stringValue)!
                        }
                        
                        self.barHours = venue?["hours"] != JSON.null ? (venue?["hours"].stringValue)! : ""
                        self.barPhone = venue?["contact"]["formattedPhone"] != JSON.null ?  (venue?["contact"]["formattedPhone"].stringValue)! : ""
                        
                        self.barPhoneToCall = venue?["contact"]["phone"] != JSON.null ? (venue?["contact"]["phone"].stringValue)! : ""
                        let arr_address = venue_location?["formattedAddress"] != JSON.null ? venue_location?["formattedAddress"].array : []
                        
                        var venue_address = ""
                        for strAddr in arr_address!
                        {
                            venue_address += " - " + strAddr.stringValue
                        }
                        self.barAddress = venue_address
                        
                        self.fillInformation()
                        
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "RestaurantDetailCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! RestaurantDetailCell
        
        let values = self.arrFeed[(indexPath as NSIndexPath).row] as! NSDictionary
        
        cell.lblDetailCell.text = values.object(forKey: "text") as? String
        cell.imgDetailCell.image = UIImage.init(named: (values.object(forKey: "image") as? String)!)
        cell.actionTap = values.object(forKey: "tap") as? String
        cell.layoutMargins = UIEdgeInsets.zero
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        
        let currentCell = tableView.cellForRow(at: indexPath!) as? RestaurantDetailCell
        
        let type : Int = Int(currentCell!.actionTap!)!
        
        switch(type)
        {
        case 1:
            self.onTapAddress()
            break
        case 2:
            self.onTapPhone((currentCell?.lblDetailCell.text)!)
            break
        case 3:
            self.onTapWebsite((currentCell?.lblDetailCell.text)!)
            break
        default:
            break
        }
    }
    
    
    func fillInformation() -> Void
    {
        if !(self.barname.isEmpty){
            let cell = ["image" : "dish_tazty_icon", "text" : String.init(format:"%@\n%@", self.barname, self.barType), "tap" : "0"]
            self.arrFeed.append(cell as AnyObject)
        }
        if !self.barAddress.isEmpty{
            let cell = ["image" : "locationRestaurante", "text" : self.barAddress, "tap" : "1"]
            self.arrFeed.append(cell as AnyObject)
        }
        if !self.barPhone.isEmpty{
            let cell = ["image" : "phoneRestaurante", "text" : self.barPhone, "tap" : "2"]
            self.arrFeed.append(cell as AnyObject)
            
        }
        if !self.barWebsite.isEmpty{
            let cell = ["image" : "websiteRestaurante", "text" : self.barWebsite, "tap" : "3"]
            self.arrFeed.append(cell as AnyObject)
        }
        
        self.tblDetails.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.tblDetails.separatorInset = UIEdgeInsets.zero
        self.tblDetails.layoutMargins = UIEdgeInsets.zero
    }
    
    func onTapWebsite(_ url: String)
    {
        let URL = Foundation.URL.init(string: url)
        UIApplication.shared.openURL(URL!)
    }
    
    func onTapAddress()
    {
        let URL = Foundation.URL.init(string: String.init(format:"http://maps.apple.com/maps?q=%f,%f", self.barLocation.latitude, self.barLocation.longitude))
        UIApplication.shared.openURL(URL!)
    }
    
    func onTapPhone(_ phone: String)
    {
        let alert = UIAlertController.init(title: "Call", message: String.init(format:"Call %@", phone), preferredStyle: .alert)
        let ok = UIAlertAction.init(title: "Call", style: .default) { (action) in
            let test = String.init(format:"tel://%@", phone.replacingOccurrences(of: " ", with: ""))
            let URL = Foundation.URL.init(string: test)
            UIApplication.shared.openURL(URL!)
            
            alert.dismiss(animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction.init(title: "Cancel", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
}
