//
//  ResturantPickerController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import SwiftyJSON

class ResturantPickerController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
    @IBOutlet weak var txtRestaurantName: UITextField!
    @IBOutlet weak var vwAddRestaurant: UIView!
    @IBOutlet weak var tblRestaurants: UITableView!
    @IBOutlet weak var btnAddRestaurant: UIButton!
    @IBOutlet weak var btnAddRestaurant2: UILabel!
    @IBOutlet weak var btnAddRestaurant3: UILabel!
    
    var videoURL : URL?
    var arrFeed = [AnyObject]()
    var restaurantSelected : Restaurant?
    var thumb : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblRestaurants.delegate = self
        self.tblRestaurants.dataSource = self
        
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        self.txtRestaurantName.delegate = self
        
        self.loadRestaurants("")
        
        self.txtRestaurantName.addTarget(self, action: #selector(ResturantPickerController.txtRestaurantNameDidChange), for: .editingChanged)

        let tap1 = UITapGestureRecognizer.init(target: self, action: #selector(ResturantPickerController.addRestaunrant(_:)))
        self.btnAddRestaurant2.isUserInteractionEnabled = true
        self.btnAddRestaurant2.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(ResturantPickerController.addRestaunrant(_:)))
        self.btnAddRestaurant3.isUserInteractionEnabled = true
        self.btnAddRestaurant3.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(ResturantPickerController.addRestaunrant(_:)))
        self.btnAddRestaurant3.isUserInteractionEnabled = true
        self.btnAddRestaurant.addGestureRecognizer(tap3)
    }
    
    func addRestaunrant(_ gestureRecognizer: UITapGestureRecognizer)
    {
        self.performSegue(withIdentifier: "segue_addrestaurant", sender: nil)
    }
    
    func loadRestaurants(_ searchText : String)
    {
        var params = [String:AnyObject]()
        if searchText.isEmpty
        {
        params = ["client_id": "5P1OVCFK0CCVCQ5GBBCWRFGUVNX5R4WGKHL2DGJGZ32FDFKT" as AnyObject,
                      "client_secret" : "UPZJO0A0XL44IHCD1KQBMAYGCZ45Z03BORJZZJXELPWHPSAR" as AnyObject,
                      "v" : "20130815" as AnyObject,
                      "ll" : String(format: "%@,%@",Session.sharedInstance.getLatitude()!,Session.sharedInstance.getLongitude()!) as AnyObject,
                      "query" : searchText as AnyObject,
                      "section" : "food" as AnyObject,
                      "sortByDistance" : "1" as AnyObject]
        }
        else
        {
            params = ["client_id": "5P1OVCFK0CCVCQ5GBBCWRFGUVNX5R4WGKHL2DGJGZ32FDFKT" as AnyObject,
                      "client_secret" : "UPZJO0A0XL44IHCD1KQBMAYGCZ45Z03BORJZZJXELPWHPSAR" as AnyObject,
                      "v" : "20130815" as AnyObject,
                      "ll" : String(format: "%@,%@",Session.sharedInstance.getLatitude()!,Session.sharedInstance.getLongitude()!) as AnyObject,
                      "query" : searchText as AnyObject,
                      "sortByDistance" : "1" as AnyObject]
        }
        
        request("https://api.foursquare.com/v2/venues/explore", method: .get, parameters: params, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result{
            case .success( _):
                if((response.result.value) != nil)
                {
                    let dic = JSON(response.result.value!)
                    
                    let supervenues = dic["response"]["groups"].array
                    let items = supervenues?.first
                    let venues = items!["items"].array
                    
                    for venue in venues!
                    {
                        let venue_location = venue["venue"]["location"]
                        let arr_address = venue_location["formattedAddress"].array
                        
                        var venue_address = ""
                        for strAddr in arr_address!
                        {
                            venue_address += " - " + strAddr.stringValue
                        }
                        
                        let item = Restaurant.init(json: nil)
                        item.barname = venue["venue"]["name"].stringValue
                        item.latitude = venue_location["lat"].stringValue
                        item.longitude = venue_location["lng"].stringValue
                        
                        let origin = CLLocationCoordinate2D.init(latitude: Double(item.latitude!)!, longitude: Double(item.longitude!)!)
                        
                        let userLocation = CLLocationCoordinate2D.init(latitude: Double(Session.sharedInstance.getLatitude()!)!, longitude: Double(Session.sharedInstance.getLongitude()!)!)
                        
                        item.distance = String(format: "%.2f", CommonFunctions.distanceMiles(origin, destination: userLocation))
                        item.videos = venue_address
                        
                        
                        self.arrFeed.append(item)
                    }
                    DispatchQueue.main.async {
                        self.tblRestaurants.reloadData()
                    }
                }
            case .failure(_): break
                //print(error)
            }
        }
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ExploreCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ExploreCell
        
        cell.lblDistance.isHidden = false
        cell.imgFollow.isHidden = true
        cell.lblNumberDishes.isHidden = false
        cell.imgCell.image = UIImage.init(named: "locationRestaurante")
        
        if indexPath.row < self.arrFeed.count
        {
        let itemRestaurant = self.arrFeed[indexPath.row] as! Restaurant
        cell.lblDistance.text = itemRestaurant.distance! + " mi"
        cell.lblCell.text = itemRestaurant.barname
        cell.lblNumberDishes.text = itemRestaurant.videos!
        }
        else
        {
            cell.lblDistance.text = ""
            cell.lblCell.text = ""
            cell.lblNumberDishes.text = ""
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if((indexPath as NSIndexPath).row >= self.arrFeed.count)
        {
            return
        }
        
        self.restaurantSelected = self.arrFeed[(indexPath as NSIndexPath).row] as? Restaurant
        self.performSegue(withIdentifier: "segue_dishpicker", sender: nil)
    }
    
    func txtRestaurantNameDidChange() {
        self.arrFeed.removeAll()
        self.loadRestaurants(self.txtRestaurantName.text!)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        if self.restaurantSelected == nil && (self.txtRestaurantName.text?.isEmpty)!{
            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "You need to pick one place from list or add new one", titleForButton: "OK", controller: self)
        }
        else
        {
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_dishpicker"
        {
            let vc = segue.destination as! DishPickerController
            vc.restaurant = self.restaurantSelected
            vc.videoURL = self.videoURL
            vc.thumb = self.thumb
        }
        else if segue.identifier == "segue_addrestaurant"
        {
            let vc = segue.destination as! AddRestaurantController
            vc.videoURL = self.videoURL
            vc.thumb = self.thumb
        }
    }
    
    
}
