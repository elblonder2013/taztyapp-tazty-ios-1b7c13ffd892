//
//  AppDelegate.swift
//  Tazty
//
//  Created by Hugo Hernandez on 2/18/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Fabric
import TwitterKit
import Crashlytics
import FBSDKLoginKit
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    enum ShortcutIdentifier: String {
        case OpenMap
        case OpenRecord
        case OpenSearch
        case OpenProfile
        
        init?(fullIdentifier: String) {
            guard let shortIdentifier = fullIdentifier.components(separatedBy: ".").last else {
                return nil
            }
            self.init(rawValue: shortIdentifier)
        }
        
        var type : String {
            return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
        }
    }
    
    var window: UIWindow?
    var launchedShortcutItem : UIApplicationShortcutItem?
    
    func handleShortCutItem(shortcutItem : UIApplicationShortcutItem) -> Bool
    {
        var handled = false
        
        guard ShortcutIdentifier(fullIdentifier: shortcutItem.type) != nil else {return false}
        guard let shortCutType = shortcutItem.type as String? else {return false}
        
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)        
        
        switch shortCutType {
        case ShortcutIdentifier.OpenMap.type:
            let vc = storyboard.instantiateViewController(withIdentifier: "MapController") as! MapController
            handled = true
            window!.rootViewController?.present(vc, animated: true, completion: nil)
            break
        case ShortcutIdentifier.OpenSearch.type:
            let vc = storyboard.instantiateViewController(withIdentifier: "SearchController") as! SearchController
            handled = true
            window!.rootViewController?.present(vc, animated: true, completion: nil)
            break
        case ShortcutIdentifier.OpenRecord.type:
            let vc = storyboard.instantiateViewController(withIdentifier: "RecordController") as! RecordController
            handled = true
            window!.rootViewController?.present(vc, animated: true, completion: nil)
            break
        case ShortcutIdentifier.OpenProfile.type:
            let vc = storyboard.instantiateViewController(withIdentifier: "ProfileController") as! ProfileController
            vc.feed_user = Session.sharedInstance.getUserID()
            handled = true
            window!.rootViewController?.present(vc, animated: true, completion: nil)
            break
        default:
            break
        }
        
        return handled
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        let handledShortCutItem = handleShortCutItem(shortcutItem: shortcutItem)
        
        completionHandler(handledShortCutItem)
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "askForNotifications"), object: nil, queue: nil) { (notification) in
            self.askForNotifications()
        }

        Twitter.sharedInstance().start(withConsumerKey: "FBC6nSCFNZNMtQomHn9KsKT82", consumerSecret: "tI0CQdxeueLGf8EAIyjZVdbHGpKnJVnfl8nrkjPrdpFiQI01n6")
        Fabric.with([Twitter.self,Crashlytics.self])
        
        UIApplication.shared.statusBarStyle = .lightContent

            if Session.sharedInstance.getLoginType() != nil && Session.sharedInstance.getLoginType() != "" {
                
                if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsKey.shortcutItem] as? UIApplicationShortcutItem {
                    
                    launchedShortcutItem = shortcutItem
                }
                else
                {
                   /* self.window = UIWindow(frame: UIScreen.main.bounds)
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    
                    let initialViewController = storyboard.instantiateViewController(withIdentifier: "HomeController") as! HomeController
                    initialViewController.isAnonymous = false
                    self.window?.rootViewController = initialViewController
                    self.window?.makeKeyAndVisible()*/
                    
                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    let storyBoard  = UIStoryboard.init(name: "StoryboardHome", bundle: nil)
                    let navigationController = storyBoard.instantiateViewController(withIdentifier: "TABBARHOME") as! TabBarController
                   
                    navigationController.selectedIndex = 0;
                    var initialViewController = navigationController.viewControllers?.first as! UINavigationController
                    let home = initialViewController.viewControllers.first as! HomeController
                    home.isAnonymous = false
                    initialViewController = navigationController.viewControllers?.last as! UINavigationController
                    let profile = initialViewController.viewControllers.first as! ProfileController
                     profile.feed_user = Session.sharedInstance.getUserID()
                    
                    //[tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"icon_tab_my_questions"] withFinishedUnselectedImage:[UIImage imageNamed:@"icon_tab_my_questions"]];
                    
                    let tabBar = navigationController.tabBar;
                    
                    
                    tabBar.items?[0].selectedImage = UIImage(named: "icon_home_selected")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[0].image = UIImage(named: "icon_home")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[0].imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
                    
                    tabBar.items?[1].selectedImage = UIImage(named: "icon_search_selected")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[1].image = UIImage(named: "icon_search")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[1].imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
                    
                    tabBar.items?[3].selectedImage = UIImage(named: "icon_heart_tab_selected")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[3].image = UIImage(named: "icon_heart_tab")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[3].imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
                    
                    tabBar.items?[4].selectedImage = UIImage(named: "icon_profile_selected")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[4].image = UIImage(named: "icon_profile_tab")!.withRenderingMode(.alwaysOriginal)
                    tabBar.items?[4].imageInsets = UIEdgeInsetsMake(6, 0, -6, 0)
                    
                    
                    let overlayView = UIScreen.main.snapshotView(afterScreenUpdates: false)
                    navigationController.view.addSubview(overlayView)
                    self.window?.rootViewController = navigationController;
                    self.window?.makeKeyAndVisible()
                    
                    
                     UIView.animate(withDuration: 0.5, delay: 0, options: .transitionCrossDissolve, animations: {
                        overlayView.alpha = 0;
                     }, completion: { (Finished) in
                        overlayView.removeFromSuperview()
                     
                     })
                    
                    
                    
                   
                    
                  
                     
                   
                    
                    
                   
                    
                  /*  navigationController
                    
                    UIStoryboard * storyBoard = [UIStoryboard storyboardWithName:@"StoryboardEstudient" bundle:nil];
                    StudentTabBarViewController *navigationController = [storyBoard instantiateViewControllerWithIdentifier:@"TABBARSTUDENT"];
                    navigationController.selectedIndex=0;
                    UITabBar *tabBar = navigationController.tabBar;
                    [GestorV getGV].altoTabBar = tabBar.frame.size.height;
                    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
                    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
                    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
                    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
                    tabBarItem1.title = @"My questions";
                    tabBarItem2.title = @"All questiones";
                    tabBarItem3.title = @"Favorites";
                    tabBarItem4.title = @"Profile";
                    [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"icon_tab_my_questions"] withFinishedUnselectedImage:[UIImage imageNamed:@"icon_tab_my_questions"]];
                    [tabBarItem2 setFinishedSelectedImage:[UIImage imageNamed:@"icon_tab_all_questions"] withFinishedUnselectedImage:[UIImage imageNamed:@"icon_tab_all_questions"]];
                    [tabBarItem3 setFinishedSelectedImage:[UIImage imageNamed:@"icon_tab_favorites"] withFinishedUnselectedImage:[UIImage imageNamed:@"icon_tab_favorites"]];
                    [tabBarItem4 setFinishedSelectedImage:[UIImage imageNamed:@"icon_tab_profile"] withFinishedUnselectedImage:[UIImage imageNamed:@"icon_tab_profile"]];
                    
                    
                    
                    
                    UIView *overlayView = [[UIScreen mainScreen] snapshotViewAfterScreenUpdates:NO];
                    [navigationController.view addSubview:overlayView];
                    self.window.rootViewController = navigationController;
                    
                    [UIView animateWithDuration:0.5f
                        delay:0.0f
                        options:UIViewAnimationOptionTransitionCrossDissolve animations:
                        ^{
                        overlayView.alpha = 0;
                        }
                        completion:^(BOOL finished)
                        {
                        [overlayView removeFromSuperview];
                        [current removeFromParentViewController];
                        }];*/
                    
                    
                }
            }

        
        application.applicationIconBadgeNumber = 0
        application.cancelAllLocalNotifications()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func askForNotifications(){
        self.registerForPushNotifications(UIApplication.shared)
    }
    
    func registerForPushNotifications(_ application: UIApplication) {
        let notificationSettings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
        
        guard let shortcut = launchedShortcutItem else {return}
        _ = handleShortCutItem(shortcutItem: shortcut)
        launchedShortcutItem = nil
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var tokenString = ""
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        
        if Session.sharedInstance.getUserID() != nil
        {
        let newPost = ["user_id": Session.sharedInstance.getUserID()!, "device_token":tokenString]
        Alamofire.request(ServicesRouter.setDeviceToken(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        print("Update device token sucessfully");
                    }
                case .failure(_):
                    print("Update device token failed");
                }
        }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print(userInfo)
    }
    
    func application(_ application: UIApplication, open url:URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        if Twitter.sharedInstance().application(application, open:url, options: [:]) {
            return true
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
}

