//
//  Feed.swift
//
//  Created by Hugo Hernandez on 6/13/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

open class Feed: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kFeedVideoKey: String = "video"
	internal let kFeedFollowingCountKey: String = "following_count"
	internal let kFeedPhotoKey: String = "photo"
	internal let kFeedThumbKey: String = "thumb"
	internal let kFeedLikesKey: String = "likes"
	internal let kFeedLikesNumKey: String = "likes_num"
	internal let kFeedFollowersCountKey: String = "followers_count"
	internal let kFeedLatKey: String = "lat"
	internal let kFeedBarnameKey: String = "barname"
	internal let kFeedLngKey: String = "lng"
	internal let kFeedMylongitudeKey: String = "mylongitude"
	internal let kFeedMylatitudeKey: String = "mylatitude"
	internal let kFeedBaraddrKey: String = "baraddr"
	internal let kFeedFullnameKey: String = "fullname"
	internal let kFeedUserIdKey: String = "user_id"
	internal let kFeedFoodnameKey: String = "foodname"
	internal let kFeedPostIdKey: String = "post_id"
	internal let kFeedPostDateKey: String = "post_date"
	internal let kFeedLikeStateKey: String = "like_state"


    // MARK: Properties
	open var video: String?
	open var followingCount: String?
	open var photo: String?
	open var thumb: String?
	open var likes: String?
	open var likesNum: String?
	open var followersCount: String?
	open var lat: String?
	open var barname: String?
	open var lng: String?
	open var mylongitude: String?
	open var mylatitude: String?
	open var baraddr: String?
	open var fullname: String?
	open var userId: String?
	open var foodname: String?
	open var postId: String?
	open var postDate: String?
	open var likeState: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		video = json[kFeedVideoKey].string
		followingCount = json[kFeedFollowingCountKey].string
		photo = json[kFeedPhotoKey].string
		thumb = json[kFeedThumbKey].string
		likes = json[kFeedLikesKey].string
		likesNum = json[kFeedLikesNumKey].string
		followersCount = json[kFeedFollowersCountKey].string
		lat = json[kFeedLatKey].string
		barname = json[kFeedBarnameKey].string
		lng = json[kFeedLngKey].string
		mylongitude = json[kFeedMylongitudeKey].string
		mylatitude = json[kFeedMylatitudeKey].string
		baraddr = json[kFeedBaraddrKey].string
		fullname = json[kFeedFullnameKey].string
		userId = json[kFeedUserIdKey].string
		foodname = json[kFeedFoodnameKey].string
		postId = json[kFeedPostIdKey].string
		postDate = json[kFeedPostDateKey].string
		likeState = json[kFeedLikeStateKey].string

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    open func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if video != nil {
			dictionary.updateValue(video! as AnyObject, forKey: kFeedVideoKey)
		}
		if followingCount != nil {
			dictionary.updateValue(followingCount! as AnyObject, forKey: kFeedFollowingCountKey)
		}
		if photo != nil {
			dictionary.updateValue(photo! as AnyObject, forKey: kFeedPhotoKey)
		}
		if thumb != nil {
			dictionary.updateValue(thumb! as AnyObject, forKey: kFeedThumbKey)
		}
		if likes != nil {
			dictionary.updateValue(likes! as AnyObject, forKey: kFeedLikesKey)
		}
		if likesNum != nil {
			dictionary.updateValue(likesNum! as AnyObject, forKey: kFeedLikesNumKey)
		}
		if followersCount != nil {
			dictionary.updateValue(followersCount! as AnyObject, forKey: kFeedFollowersCountKey)
		}
		if lat != nil {
			dictionary.updateValue(lat! as AnyObject, forKey: kFeedLatKey)
		}
		if barname != nil {
			dictionary.updateValue(barname! as AnyObject, forKey: kFeedBarnameKey)
		}
		if lng != nil {
			dictionary.updateValue(lng! as AnyObject, forKey: kFeedLngKey)
		}
		if mylongitude != nil {
			dictionary.updateValue(mylongitude! as AnyObject, forKey: kFeedMylongitudeKey)
		}
		if mylatitude != nil {
			dictionary.updateValue(mylatitude! as AnyObject, forKey: kFeedMylatitudeKey)
		}
		if baraddr != nil {
			dictionary.updateValue(baraddr! as AnyObject, forKey: kFeedBaraddrKey)
		}
		if fullname != nil {
			dictionary.updateValue(fullname! as AnyObject, forKey: kFeedFullnameKey)
		}
		if userId != nil {
			dictionary.updateValue(userId! as AnyObject, forKey: kFeedUserIdKey)
		}
		if foodname != nil {
			dictionary.updateValue(foodname! as AnyObject, forKey: kFeedFoodnameKey)
		}
		if postId != nil {
			dictionary.updateValue(postId! as AnyObject, forKey: kFeedPostIdKey)
		}
		if postDate != nil {
			dictionary.updateValue(postDate! as AnyObject, forKey: kFeedPostDateKey)
		}
		if likeState != nil {
			dictionary.updateValue(likeState! as AnyObject, forKey: kFeedLikeStateKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.video = aDecoder.decodeObject(forKey: kFeedVideoKey) as? String
		self.followingCount = aDecoder.decodeObject(forKey: kFeedFollowingCountKey) as? String
		self.photo = aDecoder.decodeObject(forKey: kFeedPhotoKey) as? String
		self.thumb = aDecoder.decodeObject(forKey: kFeedThumbKey) as? String
		self.likes = aDecoder.decodeObject(forKey: kFeedLikesKey) as? String
		self.likesNum = aDecoder.decodeObject(forKey: kFeedLikesNumKey) as? String
		self.followersCount = aDecoder.decodeObject(forKey: kFeedFollowersCountKey) as? String
		self.lat = aDecoder.decodeObject(forKey: kFeedLatKey) as? String
		self.barname = aDecoder.decodeObject(forKey: kFeedBarnameKey) as? String
		self.lng = aDecoder.decodeObject(forKey: kFeedLngKey) as? String
		self.mylongitude = aDecoder.decodeObject(forKey: kFeedMylongitudeKey) as? String
		self.mylatitude = aDecoder.decodeObject(forKey: kFeedMylatitudeKey) as? String
		self.baraddr = aDecoder.decodeObject(forKey: kFeedBaraddrKey) as? String
		self.fullname = aDecoder.decodeObject(forKey: kFeedFullnameKey) as? String
		self.userId = aDecoder.decodeObject(forKey: kFeedUserIdKey) as? String
		self.foodname = aDecoder.decodeObject(forKey: kFeedFoodnameKey) as? String
		self.postId = aDecoder.decodeObject(forKey: kFeedPostIdKey) as? String
		self.postDate = aDecoder.decodeObject(forKey: kFeedPostDateKey) as? String
		self.likeState = aDecoder.decodeObject(forKey: kFeedLikeStateKey) as? String

    }

    open func encode(with aCoder: NSCoder) {
		aCoder.encode(video, forKey: kFeedVideoKey)
		aCoder.encode(followingCount, forKey: kFeedFollowingCountKey)
		aCoder.encode(photo, forKey: kFeedPhotoKey)
		aCoder.encode(thumb, forKey: kFeedThumbKey)
		aCoder.encode(likes, forKey: kFeedLikesKey)
		aCoder.encode(likesNum, forKey: kFeedLikesNumKey)
		aCoder.encode(followersCount, forKey: kFeedFollowersCountKey)
		aCoder.encode(lat, forKey: kFeedLatKey)
		aCoder.encode(barname, forKey: kFeedBarnameKey)
		aCoder.encode(lng, forKey: kFeedLngKey)
		aCoder.encode(mylongitude, forKey: kFeedMylongitudeKey)
		aCoder.encode(mylatitude, forKey: kFeedMylatitudeKey)
		aCoder.encode(baraddr, forKey: kFeedBaraddrKey)
		aCoder.encode(fullname, forKey: kFeedFullnameKey)
		aCoder.encode(userId, forKey: kFeedUserIdKey)
		aCoder.encode(foodname, forKey: kFeedFoodnameKey)
		aCoder.encode(postId, forKey: kFeedPostIdKey)
		aCoder.encode(postDate, forKey: kFeedPostDateKey)
		aCoder.encode(likeState, forKey: kFeedLikeStateKey)

    }

}
