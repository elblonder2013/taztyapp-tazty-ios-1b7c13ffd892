//
//  FollowsController.swift
//  Tazty
//
//  Created by Victor Hernandez on 15/07/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class FollowsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblFeed: UITableView!
    
    var feedUser : String?
    var arrFeed = [People]()
    var isFollower = false
    var isFollowing = false
    var newFeedUser : String?
    var userSelected : People?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblFeed.delegate = self
        self.tblFeed.dataSource = self
        
        if self.isFollower
        {
            self.lblTitle.text = "Followers"
            self.loadFollowers()
        }
        else if self.isFollowing
        {
            self.lblTitle.text = "Followings"
            self.loadFollowings()
        }
        
                NotificationCenter.default.addObserver(self, selector: #selector(updatePeopleCell(_:)), name: NSNotification.Name(rawValue: "updatePeopleCell"), object: nil)
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)

    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.userSelected != nil{
            if UserDefaults.standard.object(forKey: "userUpdated") != nil{
                let values = UserDefaults.standard.object(forKey: "userUpdated") as! [String:AnyObject]
                
                let user = values["user"] as! String
                let youFollow = values["isFollowing"]!.boolValue!
                if user == self.userSelected?.userId
                {
                for i in 0...(self.arrFeed.count - 1)
                {
                    let newPeople = self.arrFeed[i]
                    
                    if newPeople.userId == self.userSelected?.userId
                    {
                        newPeople.isFollowing = youFollow ? "1" : "0"
                        self.arrFeed[i] = newPeople
                        
                    }
                }
                   self.tblFeed.reloadData()
                }
            }
        }
    }
    
    func loadFollowers()
    {
        let newPost : [String: AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "feedUser" : self.feedUser! as AnyObject]
        Alamofire.request(ServicesRouter.getFollowers(parameter: newPost))
        .responseJSON { response in
            switch response.result{
            case .success( _):
                if((response.result.value) != nil)
                {
                    
                    let jsonResult = JSON(response.result.value!)
                    for item in jsonResult.array!
                    {
                        let people = People.init(json: item)
                       
                            self.arrFeed.append(people)
                    
                    }
                    
                    self.tblFeed.reloadData()
                    
                }
            case .failure( let error):
                print(error)
            }
        }
    }
    
    func updatePeopleCell(_ notification : Notification)
    {
    
        let peopleInfo = (notification as NSNotification).userInfo!["cellInfo"] as! People
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        if (indexPath as NSIndexPath).row < self.arrFeed.count
        {
        self.arrFeed[(indexPath as NSIndexPath).row] = peopleInfo
        }
        self.tblFeed.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func loadFollowings()
    {
        let newPost : [String: AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "feedUser" : self.feedUser! as AnyObject]
        Alamofire.request(ServicesRouter.getFollowings(parameter: newPost))
            .responseJSON { response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        
                        let jsonResult = JSON(response.result.value!)
                        for item in jsonResult.array!
                        {
                            let people = People.init(json: item)

                                self.arrFeed.append(people)
                            
                        }
                        
                        self.tblFeed.reloadData()
                        
                    }
                case .failure( let error):
                    print(error)
                }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_follow_profile"
        {
            if sender is People
            {
                let item = sender as! People
                self.userSelected = item
                let vc = segue.destination as! ProfileController
                vc.feed_user = item.userId
            }
        }
    }
    @IBAction func onBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ExploreCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ExploreCell
        
            
            let itemPeople = self.arrFeed[(indexPath as NSIndexPath).row]
        
        cell.isPeople = true
        cell.indexPath = indexPath
        cell.item = itemPeople
        cell.viewController = self
            cell.lblDistance.isHidden = true
            cell.lblNumberDishes.isHidden = true
            cell.lblCell.text = itemPeople.fullname
            cell.imgFollow.isHidden = false
            cell.imgFollow.image = itemPeople.isFollowing == "0" ? UIImage(named:"masorange") : UIImage(named:"palomitaorange")
        if itemPeople.userId == Session.sharedInstance.getUserID()
        {
            cell.imgFollow.isHidden = true
        }
            cell.imgCell.image = UIImage.init(named: "profile_img_default")
            let url = URL(string: itemPeople.photo!)
        if url != nil
        {
            cell.imgCell.setImageWith(url!)
        }
        
            
        cell.selectionStyle = .none
        cell.configureCell()
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let item = self.arrFeed[indexPath.row] as People
//        self.newFeedUser = item.userId
//        
        //self.performSegueWithIdentifier("segue_follow_profile", sender: nil)
    }
    


}
