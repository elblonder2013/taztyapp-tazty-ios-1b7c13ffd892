//
//  HomeController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import CoreLocation
import AFNetworking
import Alamofire
import SwiftyJSON

class HomeController: UIViewController,UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var vwForAnonymous: UIView!
    @IBOutlet weak var tblFeed: UITableView!
    @IBOutlet weak var btnFollowingBorderBottom: UIButton!
    @IBOutlet weak var btnNearbyBorderBottom: UIButton!
    @IBOutlet weak var btnFollowing: UIButton!
    @IBOutlet weak var btnNearby: UIButton!
    @IBOutlet weak var vwLoading: UIView!
    @IBOutlet weak var imgLoadingGif: UIImageView!
    @IBOutlet weak var vwFollowYet: UIView!
    @IBOutlet weak var topBar: UIView!
    
    var arrFeed = [Feed]()
    let locationManager = CLLocationManager()
    var isAnonymous : Bool!
    var latitude : String?
    var longitude : String?
    let pageSize : Int = 10
    var numberPage : Int = 0
    var isInitialized = false
    var isFirstLoad = true
    var isPageRefreshing = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isInitialized = false
        self.locationManager.delegate = self
        if NSString(string:UIDevice.current.systemVersion).doubleValue > 8 {
            self.locationManager.requestAlwaysAuthorization()
        }
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        self.locationManager.startUpdatingLocation()
        NotificationCenter.default.post(name: Notification.Name(rawValue: "askForNotifications"), object: self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCellHome(_:)), name: NSNotification.Name(rawValue: "deleteCellHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCellHome(_:)), name: NSNotification.Name(rawValue: "updateCellHome"), object: nil)

        if (self.isAnonymous!)
        {
            self.configureAnonymousAccess()
        }
        self.tblFeed.delegate = self
        self.tblFeed.dataSource = self
        let loadingGif = UIImage.gifWithName("load")
        self.imgLoadingGif.image = loadingGif
        self.btnNearby.isSelected = true
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func getFeed()
    {
        let newPost : [String:AnyObject] = ["page": "\(self.numberPage)" as AnyObject, "rows": String(pageSize) as AnyObject, "userId": Session.sharedInstance.getUserID()! as AnyObject]
        Alamofire.request(ServicesRouter.getFeed(parameter: newPost))
            .responseJSON{response in
                switch response.result
                {
                case .success( _):
                    self.isPageRefreshing = false
                    
                    let jsonResult = JSON(response.result.value!)
                    
                    let arrRest = jsonResult["data"].array
                    
                    if arrRest!.count > 0
                    {
                        for item in arrRest!
                        {
                            let feedItem = Feed.init(json: item)
                            self.arrFeed.append(feedItem)
                        }
                    }
                    else
                    {
                        if self.numberPage == 0
                        {
                            self.vwFollowYet.isHidden = false
                        }
                        
                    }
                    
                    self.tblFeed.reloadData()
                    break
                case .failure( _):
                    break
                }
        }
        
    }
    
    func updateCellHome(_ notification: Notification)
    {
        let feedInfo = (notification as NSNotification).userInfo!["cellInfo"] as! Feed
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        if indexPath.row < self.arrFeed.count
        {
        self.arrFeed[indexPath.row] = feedInfo
        
        self.tblFeed.reloadData()
        }
    }
    
    func deleteCellHome(_ notification : Notification)
    {
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if indexPath.row < self.arrFeed.count
        {
        self.arrFeed.remove(at: indexPath.row)
        
        self.tblFeed.beginUpdates()
        self.tblFeed.deleteRows(at: [indexPath], with: .fade)
        self.tblFeed.endUpdates()
        }
    }
    
    func getFeedNearby()
    {
        self.vwFollowYet.isHidden = true
        
        if self.latitude == nil
        {
            self.latitude = "0"
        }
        else if self.latitude == ""
        {
            self.latitude =  "0"
        }
        if self.longitude == nil
        {
            self.longitude  = "0"
        }
        else if self.longitude == ""
        {
            self.longitude = "0"
        }
        
        let newPost : [String : AnyObject]
        if !self.isAnonymous!
        {
            newPost = ["page": "\(self.numberPage)" as AnyObject, "rows": String(pageSize) as AnyObject, "userId": Session.sharedInstance.getUserID()! as AnyObject,"latitude": self.latitude! as AnyObject, "longitude": self.longitude! as AnyObject]
        }
        else
        {
            newPost = ["page": "\(self.numberPage)" as AnyObject, "rows": String(pageSize) as AnyObject, "userId": 112 as AnyObject,"latitude": 0 as AnyObject, "longitude": 0 as AnyObject]
        }
        Alamofire.request(ServicesRouter.getFeedNearby(parameter: newPost))
            .responseJSON{response in
                switch response.result
                {
                case .success( _):
                    self.isPageRefreshing = false
                    
                    if((response.result.value) != nil)
                    {
                        
                        let jsonResult = JSON(response.result.value!)
                        
                        let arrRest = jsonResult["data"].array
                        
                        if arrRest != nil{
                            if arrRest!.count > 0
                            {
                                for item in arrRest!
                                {
                                    let feedItem = Feed.init(json: item)
                                    self.arrFeed.append(feedItem)
                                }
                            }
                            
                            self.tblFeed.reloadData()
                        }
                    }
                    break
                case .failure( _):
                    break
                }
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !self.isInitialized {
            self.isInitialized = true
            
            let userLocation: CLLocation = locations[0]
            self.latitude = String(userLocation.coordinate.latitude)
            self.longitude = String(userLocation.coordinate.longitude)
            self.locationManager.stopUpdatingLocation()
            
            Session.sharedInstance.setLatitude(self.latitude!)
            Session.sharedInstance.setLongitude(self.longitude!)
            
            
            self.getFeedNearby()
        }
    }

    func configureAnonymousAccess()
    {
        self.vwForAnonymous.isHidden = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(HomeController.handleAnonymousView(_:)))
        tap.delegate = self
        self.vwForAnonymous.addGestureRecognizer(tap)
    }
    
    func handleAnonymousView(_ sender: UITapGestureRecognizer? = nil)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showProfile(_ sender: UIButton) {
        
    }
    
    @IBAction func showSearch(_ sender: UIButton) {
        
    }
    
    @IBAction func showCamera(_ sender: UIButton) {
        
    }
    
    @IBAction func showMap(_ sender: UIButton) {
        
    }
    
    @IBAction func loadFollowingFeed(_ sender: UIButton) {
        self.tblFeed.setContentOffset(CGPoint.zero, animated:true)
        self.btnNearby.isSelected = false
        self.btnFollowing.isSelected = true
        self.btnNearbyBorderBottom.backgroundColor = UIColor.lightGray
        self.btnFollowingBorderBottom.backgroundColor = UIColor.init(red: 245/255, green: 109/255, blue: 68/255, alpha: 1)
        self.numberPage = 0
        self.arrFeed.removeAll()
        self.getFeed()
    }
    
    @IBAction func loadNearbyFeed(_ sender: UIButton) {
        self.tblFeed.setContentOffset(CGPoint.zero, animated:true)
        self.arrFeed.removeAll()
        self.btnNearby.isSelected = true
        self.numberPage = 0
        self.btnFollowing.isSelected = false
        self.btnNearbyBorderBottom.backgroundColor = UIColor.init(red: 245/255, green: 109/255, blue: 68/255, alpha: 1)
        self.btnFollowingBorderBottom.backgroundColor = UIColor.lightGray
        self.getFeedNearby()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FeedCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedCell
        
        if (indexPath as NSIndexPath).row < self.arrFeed.count
        {
            
            let feed = arrFeed[(indexPath as NSIndexPath).row]
            
            let origin = CLLocationCoordinate2D.init(latitude: Double(feed.mylatitude!)!, longitude: Double(feed.mylongitude!)!)
            
            Session.sharedInstance.setLatitude(Session.sharedInstance.getLatitude() == nil ? "0" : Session.sharedInstance.getLatitude()!)
            Session.sharedInstance.setLongitude(Session.sharedInstance.getLongitude() == nil ? "0" : Session.sharedInstance.getLongitude()!)
            
            let userLocation = CLLocationCoordinate2D.init(latitude: Double(Session.sharedInstance.getLatitude()!)!, longitude: Double(Session.sharedInstance.getLongitude()!)!)
            
            let distance = String(format: "%.2f", CommonFunctions.distanceMiles(origin, destination: userLocation))
            
            cell.indexPath = indexPath
            cell.lblFoodName.text                   = feed.foodname
            cell.lblRestaurantName.text             = feed.barname! + " - " + distance + "mi"
            cell.lblLikeCounter.titleLabel?.text    = feed.likesNum
            cell.lblUser.text                       = feed.fullname
            cell.lblTimeElapsed.text                = CommonFunctions.timeElapsed(feed.postDate!)
            
            cell.lblLikeCounter.setTitle(feed.likesNum, for: UIControlState())
            cell.lblLikeCounter.setTitleColor(feed.likeState == "0" ? Session.sharedInstance.getOrangeTazty() : UIColor.white, for: UIControlState())
            cell.lblLikeCounter.backgroundColor     = feed.likeState == "0" ? UIColor.white : Session.sharedInstance.getOrangeTazty()
            
            cell.likeButton.setImage(feed.likeState == "0" ? UIImage(named:"Tastys_empty") : UIImage(named:"Tastys_Fill"), for: UIControlState())
            
            cell.item = feed
            cell.viewController = self
            
            //Imagen
            cell.imgUser.image = UIImage(named: "profile_img_default")
            let url = URL(string: feed.photo!)
            if url != nil
            {
            cell.imgUser.setImageWith(url!)
            }
            
            //Video
            let vURLString = "http://38.100.119.14/fballr/uploads/" + feed.video!
            cell.currentURL = vURLString
            
            VideoManager.downloadVideoWithURL(vURLString)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if (indexPath as NSIndexPath).row == (tableView.indexPathsForVisibleRows?.last as NSIndexPath?)?.row
        {
            if(self.isFirstLoad)
            {
                
                self.goTableTop()
                self.isFirstLoad = false
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblFeed.contentOffset.y >= self.tblFeed.contentSize.height - self.tblFeed.bounds.size.height
        {
            if self.isPageRefreshing == false
            {
                self.isPageRefreshing = true
                self.numberPage += 10
                if self.btnNearby.isSelected
                {
                    self.getFeedNearby()
                }
                else if self.btnFollowing.isSelected
                {
                    self.getFeed()
                }
                self.tblFeed.reloadData()
            }
        }
    }
    
    func goTableTop()
    {
        self.tblFeed.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        self.vwLoading.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let feedCell : FeedCell = cell as! FeedCell
        feedCell.stopAndRemoveCurrentVideo()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_profile")
        {
            let profileController = segue.destination as! ProfileController
            if sender is UIButton
            {
                profileController.feed_user = Session.sharedInstance.getUserID()!
            }
            else if sender is Feed
            {
                let  itemFeed = sender as! Feed
                profileController.feed_user = itemFeed.userId
            }
        }
        else if segue.identifier == "segue_cell_restaurant"
        {
            if sender is Feed
            {
                let  itemFeed = sender as! Feed
                let vc = segue.destination as? RestaurantController
                vc?.countVideos = "videos"
                vc?.restaurantName = itemFeed.barname
                vc?.distance = "mi"
                
                let barlocation = CLLocationCoordinate2D.init(latitude: Double(itemFeed.mylatitude!)!, longitude: Double(itemFeed.mylongitude!)!)
                
                vc?.restaurantLocation = barlocation
            }
        }
        else if segue.identifier == "segue_home_to_dish"
        {
            if sender is Feed
            {
                let index = sender as! Feed
                
                let vc = segue.destination as! DishController
                vc.foodname = index.foodname
            }
        }
    }
}
