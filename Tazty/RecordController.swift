//
//  RecordController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import MediaPlayer
import GPUImage
import MobileCoreServices
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RecordController: UIViewController, GPUImageMovieDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var btnCapture: UIButton!
    @IBOutlet weak var vwVideo: UIView!
    @IBOutlet weak var imgGrid: UIImageView!
    @IBOutlet weak var imgFocus: UIImageView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var imgRotateAlert: UIImageView!
    @IBOutlet weak var barProgress: RecordingProgressBarView!
    
    let kNotificationDidChangeOrientation = "didChangeOrientation"
    
    var movie : GPUImageMovie?
    var movieWriter : GPUImageMovieWriter?
    var camera : GPUImageStillCamera?
    var imageView : GPUImageView?
    var rotationMode : GPUImageRotationMode?
    var cropFilter : GPUImageCropFilter?
    fileprivate var filter : GPUImageOutput?
    
    var segments = [AnyObject]()
    var recording : Bool = false
    var progress : Double?
    var recordSetting : NSMutableDictionary?
    var applied : Bool = false
    var current : NSInteger?
    var playing : Bool = false
    var playingSegment : Bool = false
    var isTooShort :Bool = false
  
    var buttons : NSMutableSet?
    
    var fCapturedTime : Double?
    
    var finalURL : URL?
    
    override func loadView() {
        super.loadView()
    
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        recordSetting?.setValue( NSNumber.init(value: kAudioFormatLinearPCM as UInt32) , forKey: AVFormatIDKey)
        recordSetting?.setValue(NSNumber.init(value: 44100.0 as Float), forKey: AVSampleRateKey)
        recordSetting?.setValue(NSNumber.init(value: 2 as Int32), forKey: AVNumberOfChannelsKey)
        recordSetting?.setValue(NSNumber.init(value: 16 as Int32), forKey: AVLinearPCMBitDepthKey)
        recordSetting?.setValue(NSNumber.init(value: false as Bool), forKey: AVLinearPCMIsBigEndianKey)
        recordSetting?.setValue(NSNumber.init(value: false as Bool), forKey: AVLinearPCMIsFloatKey)

        self.barProgress.settick(on: false)
        self.barProgress.setMaximumTime(10)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progress = 0
        self.rotationMode = kGPUImageRotateRight
        self.fCapturedTime = 0
        self.btnNext.isHidden = true
        let longGesture = UILongPressGestureRecognizer.init(target: self, action: #selector(RecordController.onLongPressGesture(_:)))
        self.btnCapture.addGestureRecognizer(longGesture)
        
        self.imageView = GPUImageView.init(frame: self.vwVideo.bounds)
        self.vwVideo.addSubview(self.imageView!)
        self.vwVideo.sendSubview(toBack: self.imageView!)
        
        cropFilter = GPUImageCropFilter.init(cropRegion: CGRect(x: 0,y: 0,width: 1,height: 0.75))
        cropFilter?.rotatedPoint(CGPoint(x: 100, y: 100), forRotation: kGPUImageRotateLeft)
        filter = GPUImageGammaFilter.init()
        camera = GPUImageStillCamera.init(sessionPreset: AVCaptureSessionPresetPhoto, cameraPosition: .back)
        camera?.horizontallyMirrorFrontFacingCamera = true
        camera?.outputImageOrientation = .portrait

        self.startCamera()
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        let tapFocus = UITapGestureRecognizer.init(target: self, action: #selector(RecordController.tapFocus(_:)))
        self.vwVideo.addGestureRecognizer(tapFocus)
        
    }
    
    func tapFocus(_ gestureRecognizer : UITapGestureRecognizer)
    {
        switch gestureRecognizer.state
        {
        case .ended:
            var point = gestureRecognizer.location(in: self.vwVideo)
            
            point.y += 70;
            self.imgFocus.center = point
            self.imgFocus.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
            
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .allowUserInteraction, animations: { 
                self.imgFocus.alpha = 1
                self.imgFocus.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                }, completion: { (finished) in
                    UIView.animate(withDuration: 0.5, delay: 0.5, options: .allowUserInteraction, animations: { 
                        self.imgFocus.alpha = 0
                        }, completion: { (finished) in
                            
                    })
            })
            
            self.focusCamera(point)
            
            break
        default:
            break
        }
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(RecordController.didChangeOrientation(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIDevice.current.endGeneratingDeviceOrientationNotifications()
        NotificationCenter.default.removeObserver(self)
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .all
    }
    
    override var preferredInterfaceOrientationForPresentation : UIInterfaceOrientation {
        return .portrait
    }
    
    func focusCamera(_ point:CGPoint)
    {
        _ = try? self.camera?.inputCamera.lockForConfiguration()
        self.camera?.inputCamera.focusPointOfInterest = point
        self.camera?.inputCamera.unlockForConfiguration()
    }
    
    func didChangeOrientation(_ notification : Notification)
    {
        let deviceOrientation = UIApplication.shared.statusBarOrientation
        camera?.outputImageOrientation = deviceOrientation
        
        if ((self.buttons == nil) || (self.buttons?.count <= 0))
        {
            return
        }
        
        self.buttons?.enumerateObjects({ (obj, stop) in
            let button : UIButton
            
            if(obj is UIButton)
            {
                button = obj as! UIButton
            }
            else
            {
                stop.pointee = true
                return
            }
            
            self.imgRotateAlert.isHidden = true
            button.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            var transform = CGAffineTransform(rotationAngle: 0)
            
            switch(UIDevice.current.orientation)
            {
            case .portrait:
                transform = CGAffineTransform(rotationAngle: 0)
                self.rotationMode = .none
                break
            case .portraitUpsideDown:
                transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                self.rotationMode = kGPUImageRotate180
                break
            case .landscapeLeft:
                transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
                self.rotationMode = kGPUImageRotateRight
                break
            case .landscapeRight:
                transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
                self.rotationMode = kGPUImageRotateLeft
                break
            default:
                break
            }
            
            
            self.cropFilter?.rotatedPoint(CGPoint(x: 160, y: 160), forRotation: self.rotationMode!)
            UIView.animate(withDuration: 0.3, animations: { 
                button.transform = transform
            })
        })
    }
    
    func onLongPressGesture(_ longGesture : UILongPressGestureRecognizer)
    {
        switch(longGesture.state)
        {
        case .began:
            if !self.applied
            {
                self.startRecording()
            }
            break
        case .ended, .cancelled, .failed:
            if !self.applied
            {
                self.stopRecording()
            }
            break
        default:
            break
        }
    }

    func startCamera()
    {
        camera?.addTarget(cropFilter)
        cropFilter?.addTarget(filter as! GPUImageInput)
        filter?.addTarget(imageView)
        camera?.startCapture()
    }
    
    func stopCamera()
    {
        camera?.stopCapture()
    }
    
    func startRecording()
    {
        let segment : URL
        
        
        self.recording = true

        if self.progress > 0
        {
            self.fCapturedTime = self.fCapturedTime! + self.progress!
        }
        
        self.progress = 0
        
        let videoPath = URL(fileURLWithPath: NSHomeDirectory())
            .appendingPathComponent(String.init(format:"tmp/movie_%@.mp4", String(Date().timeIntervalSince1970)
                .replacingOccurrences(of: ".", with: "")))
        segment = videoPath
        movieWriter = GPUImageMovieWriter.init(movieURL: segment, size: CGSize(width: 640, height: 640))
        movieWriter?.shouldPassthroughAudio = false
        movieWriter?.encodingLiveVideo = true
        
        filter?.addTarget(movieWriter)
        movieWriter?.startRecording()
        
        self.barProgress.addShot()
        self.barProgress.setIsRecording(true)
        self.barProgress.highightLastShot(forRemoval: false)
        
        self.segments.append(segment as AnyObject)
        
        self.perform(#selector(RecordController.updateRecording), with: nil, afterDelay: 0.1)
    }
    
    func stopRecording()
    {
        self.recording = false
        
        self.barProgress.setIsRecording(false)
        self.barProgress.finishShot()
        
        self.movieWriter?.finishRecording()
        self.filter?.removeTarget(movieWriter)
    }
    
    func updateRecording()
    {
        self.progress = self.progress! + 0.1
        self.barProgress.updateShot(Double(self.progress!))
        
        if self.progress! + self.fCapturedTime! >= 4.0
        {
            self.btnNext.isHidden = false
        }
        if self.progress! + self.fCapturedTime! <= 10.0
        {
            if(self.recording)
            {
                self.perform(#selector(RecordController.updateRecording), with: nil, afterDelay: 0.1)
            }
        }
    }
    
    func deleteRecording(){
        let segment = self.segments.last as! URL
        
        unlink(segment.absoluteString)
        
        self.segments.removeAll()
    }
    
    func playSegment()
    {
        let segment = self.segments[self.current!]
        if !self.playingSegment
        {
            self.playingSegment = true
            
            self.movie = GPUImageMovie.init(url: segment as! URL)
            self.movie?.delegate = self
            self.movie?.playAtActualSpeed = true
            
            self.movie?.addTarget(filter as! GPUImageInput)
            self.filter?.addTarget(self.imageView!)
            
            self.movie?.startProcessing()
        }
    }
    
    func didCompletePlayingMovie() {
        DispatchQueue.main.async { 
            self.playingSegment = false
            
            if (self.playing) && (self.current! < ((self.segments.count) - 1))
            {
                self.current = self.current! + 1
                self.playSegment()
            }
            else
            {
                self.playing = false
                
            }
        }
    }
    
    func mergeVideos()
    {
        var duration = kCMTimeZero
        var temp = kCMTimeZero
        
        let composition = AVMutableComposition.init()
        let videoTrack = composition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        for segment in self.segments
        {
            let videoAsset = AVURLAsset.init(url: segment as! URL, options: nil)
            let videoAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo)[0]
            
            do
            {
                try videoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, videoAsset.duration), of: videoAssetTrack, at: kCMTimeInvalid)
                temp = videoAsset.duration
            }
            catch _
            {
                print ("Failed load track")
            }
            
            duration = CMTimeAdd(duration, temp)
        }
        

        let filename = String.init(format: "tmp/movie_%@.mp4", String(Date().timeIntervalSince1970).replacingOccurrences(of: ".", with: ""))
        let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent(filename)
        unlink(path.absoluteString)
        let url = path

        let exportSession = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetMediumQuality)
        exportSession!.outputFileType = AVFileTypeMPEG4
        exportSession!.shouldOptimizeForNetworkUse = true  //this one optimize video to view in android
        exportSession!.outputURL = url
        
        exportSession!.exportAsynchronously(completionHandler: {
            switch(exportSession!.status)
            {
            case .failed, .cancelled:
                DispatchQueue.main.async(execute: { 
                    self.failedMerge()
                })
                break
                
            default:
                DispatchQueue.main.async(execute: {
                    self.finalURL = url
                  //  print(self.finalURL!)
                    self.completedMerge(url)
                })
                break
            }
        })
    }
    
    func completedMerge(_ url : URL)
    {
        self.performSegue(withIdentifier: "segue_videotrim", sender: nil)
    }
    
    func failedMerge()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if info[UIImagePickerControllerMediaURL] != nil
        {
        let videoURL = info[UIImagePickerControllerMediaURL] as! URL
        self.stopCamera()
        self.isTooShort = false
        let myAsset = AVURLAsset.init(url: videoURL, options: nil)
        let videoLength = CMTimeGetSeconds(myAsset.duration)
        if videoLength < 4.0
        {
            self.isTooShort = true
        }
        
        self.finalURL = videoURL
        
        self.dismiss(animated: true) {
            if self.isTooShort
            {
                DispatchQueue.main.async(execute: {
                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Video is too short. Must be 5 seconds or more", titleForButton: "OK", controller: self)
                })
                return
            }
            else
            {
            self.performSegue(withIdentifier: "segue_crop", sender: nil)
            }
        }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_videotrim"
        {
           // print(self.finalURL!)
            let vc = segue.destination as! TrimController
            vc.videoURL = self.finalURL!
        }
        else if segue.identifier == "segue_crop"
        {
            let vc = segue.destination as! CropController
            vc.urlVideo = self.finalURL!
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch!.view == self.btnCapture
        {
            if(!self.applied)
            {
                self.startRecording()
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if touch!.view == self.btnCapture
        {
            if(!self.applied)
            {
                self.stopRecording()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func nextToTrim(_ sender: UIButton) {
        self.camera?.stopCapture()
        self.camera?.removeAllTargets()
        self.cropFilter?.removeAllTargets()
        self.filter?.removeAllTargets()
        self.mergeVideos()
    }
    
    @IBAction func getFromGallery(_ sender: UIButton) {
        let picker = UIImagePickerController.init()
        picker.delegate = self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = [kUTTypeMovie as String]
        picker.videoMaximumDuration = 10.0
        
        self.present(picker, animated: true, completion: nil)
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnGrid(_ sender: UIButton) {
        self.imgGrid.isHidden = !self.imgGrid.isHidden
    }
    
    @IBAction func btnFlash(_ sender: UIButton) {
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if (device?.hasTorch)! {
            do {
                try device?.lockForConfiguration()
                if (device?.torchMode == AVCaptureTorchMode.on) {
                    device?.torchMode = AVCaptureTorchMode.off
                } else {
                    try device?.setTorchModeOnWithLevel(1.0)
                }
                device?.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
}
