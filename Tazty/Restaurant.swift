//
//  Restaurant.swift
//
//  Created by Victor Hernandez on 08/07/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

open class Restaurant: NSObject, NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kRestaurantLatitudeKey: String = "latitude"
	internal let kRestaurantVideosKey: String = "videos"
	internal let kRestaurantDistanceKey: String = "distance"
	internal let kRestaurantLongitudeKey: String = "longitude"
	internal let kRestaurantBarnameKey: String = "barname"


    // MARK: Properties
	open var latitude: String?
	open var videos: String?
	open var distance: String?
	open var longitude: String?
	open var barname: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		latitude = json[kRestaurantLatitudeKey].string
		videos = json[kRestaurantVideosKey].string
		distance = json[kRestaurantDistanceKey].string
		longitude = json[kRestaurantLongitudeKey].string
		barname = json[kRestaurantBarnameKey].string

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    open func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if latitude != nil {
			dictionary.updateValue(latitude! as AnyObject, forKey: kRestaurantLatitudeKey)
		}
		if videos != nil {
			dictionary.updateValue(videos! as AnyObject, forKey: kRestaurantVideosKey)
		}
		if distance != nil {
			dictionary.updateValue(distance! as AnyObject, forKey: kRestaurantDistanceKey)
		}
		if longitude != nil {
			dictionary.updateValue(longitude! as AnyObject, forKey: kRestaurantLongitudeKey)
		}
		if barname != nil {
			dictionary.updateValue(barname! as AnyObject, forKey: kRestaurantBarnameKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.latitude = aDecoder.decodeObject(forKey: kRestaurantLatitudeKey) as? String
		self.videos = aDecoder.decodeObject(forKey: kRestaurantVideosKey) as? String
		self.distance = aDecoder.decodeObject(forKey: kRestaurantDistanceKey) as? String
		self.longitude = aDecoder.decodeObject(forKey: kRestaurantLongitudeKey) as? String
		self.barname = aDecoder.decodeObject(forKey: kRestaurantBarnameKey) as? String

    }

    open func encode(with aCoder: NSCoder) {
		aCoder.encode(latitude, forKey: kRestaurantLatitudeKey)
		aCoder.encode(videos, forKey: kRestaurantVideosKey)
		aCoder.encode(distance, forKey: kRestaurantDistanceKey)
		aCoder.encode(longitude, forKey: kRestaurantLongitudeKey)
		aCoder.encode(barname, forKey: kRestaurantBarnameKey)

    }

}
