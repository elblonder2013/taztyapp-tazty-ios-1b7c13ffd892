//
//  RestaurantDetailCell.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/26/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit

class RestaurantDetailCell: UITableViewCell {

    @IBOutlet weak var imgDetailCell: UIImageView!
    @IBOutlet weak var lblDetailCell: UILabel!
    var actionTap: String?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
