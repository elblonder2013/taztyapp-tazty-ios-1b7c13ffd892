//
//  RestaurantController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/25/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class RestaurantController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    var distance : String!
    var countVideos : String!
    var restaurantName: String!
    var restaurantLocation : CLLocationCoordinate2D!
    var arrFeed = [Feed]()
    var isFirstLoad = true
    var numberPage : Int = 0
    var isPageRefreshing = false
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitle: UILabel!
    @IBOutlet weak var tblFeed: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblTitle.text = restaurantName
        
        self.tblFeed.delegate = self
        self.tblFeed.dataSource = self
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        let newPost = ["barname" : restaurantName, "latitude" : Session.sharedInstance.getLatitude()! as String, "longitude" : Session.sharedInstance.getLongitude()! as String, "page" : "0", "rows" : "10"] as [String : Any]
        Alamofire.request(ServicesRouter.searchRestaurant(parameter: newPost))
            .responseJSON{ response in
                switch response.result{
                case .success( _):
                    let jsonResult = JSON(response.result.value!)
                    
                    for item in jsonResult.array!
                    {
                        let itemRes = Restaurant.init(json: item)
                        self.lblSubtitle.text = String.init(format:"%@ mi | %@ videos | Business info", itemRes.distance!, itemRes.videos!)
                        
                    }
                    
                    break
                case .failure( _):
                    break
                }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCellHome(_:)), name: NSNotification.Name(rawValue: "deleteCellHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCellHome(_:)), name: NSNotification.Name(rawValue: "updateCellHome"), object: nil)
        
        self.lblSubtitle.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(RestaurantController.goToDetail(_:)))
        self.lblSubtitle.addGestureRecognizer(tap)
        
        
        self.loadFeed()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func goToDetail(_ gestureRecognizer : UITapGestureRecognizer)
    {
        self.performSegue(withIdentifier: "segue_detailRestaurant", sender: nil)
    }
    
    func updateCellHome(_ notification: Notification)
    {
        let feedInfo = (notification as NSNotification).userInfo!["cellInfo"] as! Feed
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if (indexPath as NSIndexPath).row < self.arrFeed.count
        {
        self.arrFeed[(indexPath as NSIndexPath).row] = feedInfo
        
        self.tblFeed.reloadData()
        }
    }
    
    func deleteCellHome(_ notification : Notification)
    {
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if indexPath.row < self.arrFeed.count
        {
        self.arrFeed.remove(at: indexPath.row)
        
        self.tblFeed.beginUpdates()
        self.tblFeed.deleteRows(at: [indexPath], with: .fade)
        self.tblFeed.endUpdates()
        }
    }
    
    func loadFeed()
    {
        let newPost = ["barname" : restaurantName, "userId": Session.sharedInstance.getUserID()! as String, "page" : "\(self.numberPage)", "rows" : "10"] as [String : Any]
        Alamofire.request(ServicesRouter.getFeedByBar(parameter: newPost))
            .responseJSON{ response in
                switch response.result{
                case .success( _):
                    let jsonResult = JSON(response.result.value!)
                        for itemFeed in jsonResult["data"].array!{
                            let feedItem = Feed.init(json: itemFeed)
                            self.arrFeed.append(feedItem)
                            
                        }
                        self.tblFeed.reloadData()
                    
                    self.isPageRefreshing = false
                    break
                case .failure( _):
                    break
                }
        }
    }
    
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionBack(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func actionDetail(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segue_detailRestaurant", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_detailRestaurant")
        {
            let vc = segue.destination as? DetailRestaurantController
            vc?.barLocation = restaurantLocation
            vc?.barname = restaurantName
        }
        if segue.identifier == "segue_restaurant_profile"
        {
            if sender is Feed
            {
                let item  = sender as! Feed
                let vc  = segue.destination as? ProfileController
                vc?.feed_user = item.userId
            }
        }
        else if segue.identifier == "segue_profile_to_dish"
        {
            if sender is Feed
            {
                let index = sender as! Feed
                
                let vc = segue.destination as! DishController
                vc.foodname = index.foodname
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 430
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FeedCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedCell
        
        let feed = arrFeed[(indexPath as NSIndexPath).row]
        
        let origin = CLLocationCoordinate2D.init(latitude: Double(feed.mylatitude!)!, longitude: Double(feed.mylongitude!)!)
        
        let userLocation = CLLocationCoordinate2D.init(latitude: Double(Session.sharedInstance.getLatitude()!)!, longitude: Double(Session.sharedInstance.getLongitude()!)!)
        
        let distance2 = String(format: "%.2f", CommonFunctions.distanceMiles(origin, destination: userLocation))
        
        cell.indexPath = indexPath
        cell.lblFoodName.text                   = feed.foodname
        cell.lblRestaurantName.text             = feed.barname! + " - " + distance2 + "mi"
        cell.lblLikeCounter.titleLabel?.text    = feed.likesNum
        cell.lblUser.text                       = feed.fullname
        cell.lblTimeElapsed.text                = CommonFunctions.timeElapsed(feed.postDate!)
        
        cell.lblLikeCounter.setTitle(feed.likesNum, for: UIControlState())
        cell.lblLikeCounter.setTitleColor(feed.likeState == "0" ? Session.sharedInstance.getOrangeTazty() : UIColor.white, for: UIControlState())
        cell.lblLikeCounter.backgroundColor     = feed.likeState == "0" ? UIColor.white : Session.sharedInstance.getOrangeTazty()
        
        cell.likeButton.setImage(feed.likeState == "0" ? UIImage(named:"Tastys_empty") : UIImage(named:"Tastys_Fill"), for: UIControlState())

        
        cell.item = feed
        cell.viewController = self
        
        //Imagen
        cell.imgUser.image = UIImage(named: "profile_img_default")
        let url = URL(string: feed.photo!)
        if url != nil
        {
        cell.imgUser.setImageWith(url!)
        }
        
        //Video
        let vURLString = "http://38.100.119.14/fballr/uploads/" + feed.video!
        cell.currentURL = vURLString
        
        VideoManager.downloadVideoWithURL(vURLString)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if (indexPath as NSIndexPath).row == (tableView.indexPathsForVisibleRows?.last as NSIndexPath?)?.row
        {
            if(self.isFirstLoad)
            {
                self.goTableTop()
                self.isFirstLoad = false            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.arrFeed.count>=10
        {
        if self.tblFeed.contentOffset.y >= self.tblFeed.contentSize.height - self.tblFeed.bounds.size.height
        {
            if self.isPageRefreshing == false
            {
                self.isPageRefreshing = true
                self.numberPage += 10
                self.loadFeed()
                self.tblFeed.reloadData()
            }
        }
        }
    }
    
    
    func goTableTop()
    {
        self.tblFeed.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let feedCell : FeedCell = cell as! FeedCell
        feedCell.stopAndRemoveCurrentVideo()
    }

}
