//
//  CropController.swift
//  Tazty
//
//  Created by Victor Hernandez on 10/07/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Player

class CropController: UIViewController, PlayerDelegate {
    
    @IBOutlet weak var vwSource: UIScrollView!
    @IBOutlet weak var vwPlayback: UIView!
    @IBOutlet weak var vwVideo: UIView!
    
    fileprivate var player: Player!
    
    var urlVideo : URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        self.player = Player()
        self.player.playerDelegate = self
        self.player.url = self.urlVideo!
        self.player.playbackLoops = true
        self.player.view.frame = CGRect(x: 0, y: 0, width: self.vwVideo.frame.size.width , height: self.vwVideo.frame.height)
        self.player.muted = true
        
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.player.view.addGestureRecognizer(tapGestureRecognizer)
        
        self.addChildViewController(self.player)
        self.vwVideo.addSubview(self.player.view)
        self.player.didMove(toParentViewController: self)
        self.player.playFromBeginning()
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(CropController.handlePan(_:)))
        self.vwPlayback.addGestureRecognizer(gestureRecognizer)
        
        self.vwPlayback.layer.borderWidth = 3
        self.vwPlayback.layer.borderColor = Session.sharedInstance.getOrangeTazty()?.cgColor
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func onNext(_ sender: AnyObject) {

                self.performSegue(withIdentifier: "segue_crop_trim", sender: nil)

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.player.playFromBeginning()
    }
    
    @IBAction func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizerState.began || gestureRecognizer.state == UIGestureRecognizerState.changed {
            
            let translation = gestureRecognizer.translation(in: self.view)
            // note: 'view' is optional and need to be unwrapped

            gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
            gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)

        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_crop_trim")
        {
            let vc = segue.destination as! TrimController
            vc.videoURL = self.urlVideo
        }
    }


    @IBAction func onBack(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        switch (self.player.playbackState.rawValue) {
        case PlaybackState.stopped.rawValue:
            self.player.playFromBeginning()
        case PlaybackState.paused.rawValue:
            self.player.playFromCurrentTime()
        case PlaybackState.playing.rawValue:
            self.player.pause()
        case PlaybackState.failed.rawValue:
            self.player.pause()
        default:
            self.player.pause()
        }
    }
    
    func playerReady(_ player: Player) {
    }
    
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    
    func playerBufferingStateDidChange(_ player: Player) {
    }
    
    func playerPlaybackWillStartFromBeginning(_ player: Player) {
    }
    
    func playerPlaybackDidEnd(_ player: Player) {
    }
    
    func playerCurrentTimeDidChange(_ player: Player) {
    }
    
}
