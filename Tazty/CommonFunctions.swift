//
//  CommonFunctions.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/12/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import Foundation
import SystemConfiguration
import MapKit
import Alamofire
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CommonFunctions
{
    class func showAlertWithTitle(_ titleForAlert: String?, messageForAlert: String?, titleForButton: String?, controller: UIViewController)
    {
        let alertController = UIAlertController(title: titleForAlert, message:
            messageForAlert, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: titleForButton, style: UIAlertActionStyle.default,handler: nil))
        
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func validateEmail(_ emailToTest: String?) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: emailToTest)
    }
    
    class func distanceMiles(_ origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) -> CLLocationDistance
    {
        let orgLocation = CLLocation.init(latitude: origin.latitude, longitude: origin.longitude)
        let curLocation = CLLocation.init(latitude: destination.latitude, longitude: destination.longitude)
        
        var meter = curLocation.distance(from: orgLocation)
        meter = meter / 1609.34
        return meter
    }
    
    class func timeElapsed(_ postDate : String) -> String
    {

        let numberOfMinutes =  Int(postDate)
        
        if numberOfMinutes != nil
        {
            if numberOfMinutes > 0
            {
                let numberOfHours = numberOfMinutes! > 60 ? numberOfMinutes! / 60 : 0;
                let numberOfDays = numberOfHours > 24 ? numberOfHours / 24 : 0;
                let numberOfMonths = numberOfDays > 30 ? numberOfDays / 30 : 0;
                
                        if numberOfMonths > 0
                        {
                            if numberOfMonths > 12
                            {
                                return String(format: "%d yr", numberOfMonths / 12)
                            }
                            return String(format: "%d mo", numberOfMonths)
                        }
                        else if numberOfDays > 0
                        {
                            if numberOfDays > 7
                            {
                                let numberOfWeeks = numberOfDays / 7
                                return String(format:"%d w", numberOfWeeks)
                            }
                            return String(format:"%d d", numberOfDays)
                        }
                        else if numberOfHours > 0
                        {
                            return String(format:"%d h", numberOfHours)
                        }
                        else if numberOfMinutes > 0
                        {
                            return String(format:"%d min", numberOfMinutes!)
                        }
                        else if numberOfMinutes == 0
                        {
                            return String(format:"0 min")
                        }
            }
            else
            {
                return "0 min";
            }
        }
        else
        {
            return "";
        }
        
        return ""
    }
    
}

extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}
