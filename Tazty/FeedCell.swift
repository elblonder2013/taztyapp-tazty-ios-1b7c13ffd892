//
//  FeedCell.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer
import Alamofire
import SwiftyJSON

class FeedCell: UITableViewCell {

    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblRestaurantName: UILabel!
    @IBOutlet weak var lblTimeElapsed: UILabel!
    @IBOutlet weak var imgPlay: UIImageView!
    @IBOutlet weak var vwVideo: UIView!
    @IBOutlet weak var lblLikeCounter: UIButton!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var imgMoreActions: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    
    //Codigo de frank: Permitido tocar solo para magos nivel 89 o superior
    var player = AVPlayer()
    var currentURL : String?
    var vlayer : AVPlayerLayer?
    var item : Feed?
    var viewController : UIViewController?
    var indexPath : IndexPath?
    
    func changeVideoWithNSURL(_ videoPath: URL){
        DispatchQueue.global().async { 
            let asset : AVURLAsset = AVURLAsset(url: videoPath)
            let playerItem : AVPlayerItem = AVPlayerItem(asset: asset)
            DispatchQueue.main.async(execute: {
                self.player.replaceCurrentItem(with: playerItem)
            })
        }
    }
    //************************************************************
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //TODO: remmeplazar vista por player
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        player.actionAtItemEnd = AVPlayerActionAtItemEnd.none
        player.isMuted = true
        vlayer = AVPlayerLayer(player: self.player)
       
        vlayer?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: self.vwVideo.frame.height)
        vlayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.vwVideo.layer.addSublayer(vlayer!)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedCell.newVideoLoaded(_:)), name: NSNotification.Name(rawValue: "cKNewVideoLoaded"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedCell.playerDidEndPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player.currentItem)
        NotificationCenter.default.addObserver(self, selector: #selector(FeedCell.playerItemPlaybackStalledNotification(_:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: self.player)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(FeedCell.moreActions(_:)))
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(FeedCell.goToProfile))
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(FeedCell.goToRestaurant))
        
        let tap4 = UITapGestureRecognizer(target: self, action: #selector(FeedCell.goToDish))
        
        self.imgMoreActions.isUserInteractionEnabled = true
        self.imgMoreActions.addGestureRecognizer(tap1)
        
        self.imgUser.isUserInteractionEnabled = true
        self.imgUser.addGestureRecognizer(tap2)
        
        self.lblRestaurantName.isUserInteractionEnabled = true
        self.lblRestaurantName.addGestureRecognizer(tap3)
        
        self.lblFoodName.isUserInteractionEnabled = true
        self.lblFoodName.addGestureRecognizer(tap4)
    }
    
    func goToDish(_ gestureRecognizer: UITapGestureRecognizer)
    {
        if viewController!.topMostViewController() is HomeController
        {
            self.viewController?.performSegue(withIdentifier: "segue_home_to_dish", sender: item)
        }
        else if viewController!.topMostViewController() is RestaurantController
        {
            self.viewController?.performSegue(withIdentifier: "segue_restaurant_to_dish", sender: item)
        }
        else if viewController!.topMostViewController() is ProfileController
        {
            self.viewController?.performSegue(withIdentifier: "segue_profile_to_dish", sender: item)
        }
    }
    
    func goToRestaurant(_ gestureRecognizer: UITapGestureRecognizer)
    {
        if viewController!.topMostViewController() is HomeController
        {
        self.viewController?.performSegue(withIdentifier: "segue_cell_restaurant", sender: item)
        }
        else if viewController!.topMostViewController() is ProfileController
        {
            self.viewController?.performSegue(withIdentifier: "segue_profile_restaurant", sender: item)
        }
        else if viewController!.topMostViewController() is DishController
        {
            self.viewController?.performSegue(withIdentifier: "segue_dish_to_restaurant", sender: item)
        }
    }
    
    func goToProfile(_ gestureRecognizer: UITapGestureRecognizer)
    {
        if item?.userId != Session.sharedInstance.getUserID() && viewController!.topMostViewController() is HomeController
        {
        self.viewController?.performSegue(withIdentifier: "segue_profile", sender: item)
        }
        else if item?.userId != Session.sharedInstance.getUserID() && viewController!.topMostViewController() is RestaurantController
        {
            self.viewController?.performSegue(withIdentifier: "segue_restaurant_profile", sender: item)
        }
        else if viewController!.topMostViewController() is DishController
        {
            self.viewController?.performSegue(withIdentifier: "segue_dish_to_profile", sender: item)
        }
    }
    
    func moreActions(_ gestureRecognizer : UITapGestureRecognizer){
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        if Session.sharedInstance.getUserID() != self.item?.userId
        {
            let reportAction = UIAlertAction(title: "Report Inappropriate", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let newPost : [String:AnyObject] = ["userId": Session.sharedInstance.getUserID()! as AnyObject, "postId": self.item!.postId! as AnyObject]
                Alamofire.request(ServicesRouter.report(parameter: newPost))
                    .responseJSON{response in
                        switch response.result{
                        case .success( _):
                            //let jsonResult = JSON(response.result.value!)
                            
                            DispatchQueue.main.async(execute: {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "deleteCellHome"), object: nil, userInfo: ["indexPath":self.indexPath!])
                                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Your report was received by Tazty team, thanks!", titleForButton: "OK", controller: self.viewController!)
                            })
                            
                            break
                        case .failure( _):
                            break
                        }
                        
                }
                
            })
            optionMenu.addAction(reportAction)
        }
        else
        {
            let deleteAction = UIAlertAction(title: "Delete Post", style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                let newPost : [String:AnyObject] = ["userId": Session.sharedInstance.getUserID()! as AnyObject, "postId": self.item!.postId! as AnyObject]
                Alamofire.request(ServicesRouter.deletePost(parameter: newPost))
                    .responseJSON{response in
                        switch response.result{
                        case .success( _):
                            //let jsonResult = JSON(response.result.value!)
                            
                            DispatchQueue.main.async(execute: {
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "deleteCellHome"), object: nil, userInfo: ["indexPath":self.indexPath!])
                                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Your video was sucessfully deleted", titleForButton: "OK", controller: self.viewController!)
                            })
                            
                            break
                        case .failure( _):
                            break
                        }
                        
                }

                
            })
            optionMenu.addAction(deleteAction)
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        optionMenu.addAction(cancelAction)
        
        viewController!.present(optionMenu, animated: true, completion: nil)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.imgUser.layer.cornerRadius = self.imgUser.frame.width / 2
        self.imgUser.clipsToBounds = true
        self.imgUser.layer.borderWidth = 1
        self.imgUser.layer.borderColor = UIColor.white.cgColor
        self.imgUser.layer.masksToBounds = true
        
        self.lblLikeCounter.layer.cornerRadius = self.lblLikeCounter.frame.size.height / 2
        self.lblLikeCounter.clipsToBounds = true
        self.lblLikeCounter.layer.borderColor = UIColor.init(red: 243/255, green: 101/255, blue: 35/255, alpha: 1).cgColor
        self.lblLikeCounter.layer.borderWidth = 1
        
         
        //CGRectMake(0, 0, vwVideo.frame.size.width, vwVideo.frame.size.height)
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnPlay(_ sender: UIButton) {
        
    }
    
    @IBAction func btnLike(_ sender: UIButton) {

        
        for _ in 0...20 {
            let square = UIImageView()
            square.frame = CGRect(x: 55, y: 300, width: 20, height: 20)
            square.backgroundColor = UIColor.clear
            square.image = UIImage(named:"dish_tazty_icon")
            self.viewController?.view.addSubview(square)
            
            let position = self.likeButton.superview?.convert(self.likeButton.frame, to: self.viewController!.view)
            
            let randomYOffset = CGFloat( arc4random_uniform(150))
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: (position?.origin.x)!,y: (position?.origin.y)!))
            path.addCurve(to: CGPoint(x: 301, y: 239 + randomYOffset), controlPoint1: CGPoint(x: 136, y: 373 + randomYOffset), controlPoint2: CGPoint(x: 178, y: 110 + randomYOffset))
            
            let anim = CAKeyframeAnimation(keyPath: "position")
            anim.path = path.cgPath
            anim.rotationMode = kCAAnimationRotateAuto
            anim.repeatCount = Float.infinity
            anim.duration = Double(arc4random_uniform(40)+30) / 10
            anim.timeOffset = Double(arc4random_uniform(290))
            square.layer.add(anim, forKey: "animate position along path")
            
            let delay = 1 * Double(NSEC_PER_SEC)
            let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                UIView.transition(with: square, duration: 0.1, options: .transitionCrossDissolve, animations: {
                    square.removeFromSuperview()
                    }, completion: nil)
            })
        }
        
        if item?.likeState == "0"
        {
            let newPost : [String:AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "postId" : (item?.postId)! as AnyObject]
            Alamofire.request(ServicesRouter.like(parameter: newPost))
                .responseJSON{response in
                    switch response.result{
                    case .success( _):
                        self.likeButton.setImage(UIImage(named:"Tastys_Fill"), for: UIControlState())
                        self.item?.likeState = "1"
                        self.item?.likesNum = String(Int((self.item?.likesNum)!)! + 1)

                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateCellHome"), object: nil, userInfo: ["cellInfo" : self.item!, "indexPath" : self.indexPath!])

                        break
                    case .failure( _):
                        
                        break
                    }
            }
            
        }
        else if item?.likeState == "1"
        {
            let newPost : [String:AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "postId" : (item?.postId)! as AnyObject]
            Alamofire.request(ServicesRouter.unlike(parameter: newPost))
                .responseJSON{response in
                    switch response.result{
                    case .success( _):
                        self.likeButton.setImage(UIImage(named:"Tastys_empty"), for: UIControlState())
                        self.item?.likeState = "0"
                        self.item?.likesNum = String(Int((self.item?.likesNum)!)! - 1)
                        if self.viewController!.topMostViewController() is HomeController
                        {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateCellHome"), object: nil, userInfo: ["cellInfo" : self.item!, "indexPath" : self.indexPath!])
                        }
                        else if self.viewController!.topMostViewController() is RestaurantController
                        {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateCellRestaurant"), object: nil, userInfo: ["cellInfo" : self.item!, "indexPath" : self.indexPath!])
                        }
                        else if self.viewController!.topMostViewController() is DishController
                        {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateCellDish"), object: nil, userInfo: ["cellInfo" : self.item!, "indexPath" : self.indexPath!])
                        }
                        else if self.viewController!.topMostViewController() is ProfileController
                        {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateCellProfile"), object: nil, userInfo: ["cellInfo" : self.item!, "indexPath" : self.indexPath!])
                        }
                        break
                    case .failure( _):
                        
                        break
                    }
            }
            
        }
        

    }
    
    func stopAndRemoveCurrentVideo(){
        self.player.pause()
        self.player.currentItem?.cancelPendingSeeks()
        self.player.currentItem?.asset.cancelLoading()
        self.player.replaceCurrentItem(with: nil)
    }

}

extension FeedCell {
    
    func newVideoLoaded(_ note: Notification){
        let arrayData : NSArray = note.object as! NSArray
        //Hacer esto una entidad
        let url = arrayData[0] as? String
        let path = arrayData[1] as! URL
        
        if currentURL == url {
            self.changeVideoWithNSURL(path)
            self.player.play()
        }
    }
    
    func playerDidEndPlaying(_ note: Notification){
        let currentItem : AVPlayerItem = note.object as! AVPlayerItem
        currentItem.seek(to: kCMTimeZero)
        self.player.play()
    }
    
    func playerItemPlaybackStalledNotification(_ note: Notification){ self.player.play() }
}
