//
//  SettingsController.swift
//  Tazty
//
//  Created by Victor Hernandez on 01/07/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit

class SettingsController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblSettings: UITableView!
    var items: [String] = ["About","Support","Contact Us","Terms of Service","Privacy Policy","","Logout"]
    var action: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblSettings.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tblSettings.delegate = self
        self.tblSettings.dataSource = self
        self.tblSettings.tableFooterView = UIView()
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = self.tblSettings.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[(indexPath as NSIndexPath).row]
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        
        
        if (indexPath as NSIndexPath).row == 6 {
            cell.textLabel?.textColor = UIColor.init(red: 243/255, green: 101/255, blue: 35/255, alpha: 1)
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath as NSIndexPath).row{
        case 0:action = 1
            self.performSegue(withIdentifier: "segue_showSettings", sender: nil)
            break
        case 1:action = 2
        self.performSegue(withIdentifier: "segue_showSettings", sender: nil)
            break
        case 2:action = 3
        self.performSegue(withIdentifier: "segue_showSettings", sender: nil)
            break
        case 3:action = 4
        self.performSegue(withIdentifier: "segue_showSettings", sender: nil)
            break
        case 4:action = 5
        self.performSegue(withIdentifier: "segue_showSettings", sender: nil)
            break
        case 5:
            break
        case 6:
        let alert = UIAlertController(title: "Tazty", message: "Are you sure you want to log out?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            
            Session.sharedInstance.setUserID("")
            Session.sharedInstance.setPhoto("")
            Session.sharedInstance.setUsername("")
            Session.sharedInstance.setLoginType("")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OpenController") as! OpenController
            self.present(vc, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_showSettings")
        {
            switch action
            {
            case 1:let vc = segue.destination as? InfoGenericController
            vc?.textToTitle = "About"
            vc?.urlToShow = "http://taztyapp.com/phone/about-app.html"
                break
            case 2:let vc = segue.destination as? InfoGenericController
            vc?.textToTitle = "Support"
            vc?.urlToShow = "http://taztyapp.com/phone/support-app.html"
                break
            case 3:let vc = segue.destination as? InfoGenericController
            vc?.textToTitle = "Contact Us"
            vc?.urlToShow = "http://taztyapp.com/phone/contact-us-app.html"
                break
            case 4:let vc = segue.destination as? InfoGenericController
            vc?.textToTitle = "Terms of Service"
            vc?.urlToShow = "http://taztyapp.com/phone/terms-of-service.html"
                break
            case 5:let vc = segue.destination as? InfoGenericController
            vc?.textToTitle = "Privacy Policy"
            vc?.urlToShow = "http://taztyapp.com/phone/privacy-policy.html"
                break
            default:
                break
            }
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
