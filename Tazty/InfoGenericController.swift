//
//  InfoGenericController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/12/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit

class InfoGenericController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var wvGeneric: UIWebView!
    
    var textToTitle: String?
    var urlToShow: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = self.textToTitle
        wvGeneric.delegate = self
        loadWebPage()
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnBackPrevious(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadWebPage(){
    let url = URL(string: urlToShow!)
    let request = URLRequest(url: url!)
    wvGeneric.loadRequest(request)
}

    func webViewDidStartLoad(_ webView: UIWebView){
UIApplication.shared.isNetworkActivityIndicatorVisible = true
}

    func webViewDidFinishLoad(_ webView: UIWebView){
UIApplication.shared.isNetworkActivityIndicatorVisible = false
}

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
UIApplication.shared.isNetworkActivityIndicatorVisible = false

}

}
