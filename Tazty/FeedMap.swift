//
//  FeedMap.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/25/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import Foundation
import SwiftyJSON

open class FeedMap: NSObject, NSCoding{
    internal let kFeedMapDishesKey: String = "dishes"
    internal let kFeedMapLatitudeKey: String = "latitude"
    internal let kFeedMapLongitudeKey: String = "longitude"
    internal let kFeedMapBarnameKey: String = "barname"
    internal let kFeedMapDistanceKey: String = "distance"
    
    open var dishes: Int?
    open var latitude: String?
    open var longitude: String?
    open var barname: String?
    open var distance: String?
    
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }
    
    public init(json: JSON) {
        dishes = json[kFeedMapDishesKey].int
        latitude = json[kFeedMapLatitudeKey].string
        longitude = json[kFeedMapLongitudeKey].string
        barname = json[kFeedMapBarnameKey].string
        distance = json[kFeedMapDistanceKey].string
        
    }

    open func dictionaryRepresentation() -> [String : AnyObject ] {
        
        var dictionary: [String : AnyObject ] = [ : ]
        if dishes != nil {
            dictionary.updateValue(dishes! as AnyObject, forKey: kFeedMapDishesKey)
        }
        if latitude != nil {
            dictionary.updateValue(latitude! as AnyObject, forKey: kFeedMapLatitudeKey)
        }
        if longitude != nil {
            dictionary.updateValue(longitude! as AnyObject, forKey: kFeedMapLongitudeKey)
        }
        if barname != nil {
            dictionary.updateValue(barname! as AnyObject, forKey: kFeedMapBarnameKey)
        }
        if distance != nil {
            dictionary.updateValue(distance! as AnyObject, forKey: kFeedMapDistanceKey)
        }
        
        return dictionary
    }
    
    required public init(coder aDecoder: NSCoder) {
        self.dishes = aDecoder.decodeObject(forKey: kFeedMapDishesKey) as? Int
        self.latitude = aDecoder.decodeObject(forKey: kFeedMapLatitudeKey) as? String
        self.longitude = aDecoder.decodeObject(forKey: kFeedMapLongitudeKey) as? String
        self.barname = aDecoder.decodeObject(forKey: kFeedMapBarnameKey) as? String
        self.distance = aDecoder.decodeObject(forKey: kFeedMapDistanceKey) as? String
    }
    
    open func encode(with aCoder: NSCoder) {
        aCoder.encode(dishes, forKey: kFeedMapDishesKey)
        aCoder.encode(latitude, forKey: kFeedMapLatitudeKey)
        aCoder.encode(longitude, forKey: kFeedMapLongitudeKey)
        aCoder.encode(barname, forKey: kFeedMapBarnameKey)
        aCoder.encode(distance, forKey: kFeedMapDistanceKey)
    }
}
