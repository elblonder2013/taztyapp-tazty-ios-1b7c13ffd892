//
//  DishPickerController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DishPickerController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var txtFoodName: UITextField!
    @IBOutlet weak var tblDishes: UITableView!
    
    var videoURL : URL?
    var restaurant : Restaurant?
    var arrFeed = [Any]()
    var selectedDish : String?
    var thumb : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tblDishes.delegate = self
        self.tblDishes.dataSource = self

        // Do any additional setup after loading the view.
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        self.txtFoodName.delegate = self
        
        self.loadDishes("")
        
        self.txtFoodName.addTarget(self, action: #selector(ResturantPickerController.txtRestaurantNameDidChange), for: .editingChanged)
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadDishes(_ foodname : String)
    {
        let newPost = ["foodname": foodname]
        Alamofire.request(ServicesRouter.searchDishes(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        self.arrFeed.removeAll()
                        let jsonResult = JSON(response.result.value!)
                        for item in jsonResult.array!
                        {
                            let foodname = item.stringValue
                            if(!foodname.isEmpty)
                            {
                                self.arrFeed.append(foodname)
                            }
                        }
                        
                        self.tblDishes.reloadData()
                        
                    }
                case .failure( let error):
                    print(error)
                }
        }

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ExploreCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ExploreCell
        
        if indexPath.row < self.arrFeed.count
        {
            let itemDish = self.arrFeed[indexPath.row] as! String
            cell.lblCell.text = itemDish
        }
        else
        {
            cell.lblCell.text = ""
        }
        cell.lblDistance.isHidden = true
        cell.lblNumberDishes.isHidden = true
        
        cell.imgFollow.isHidden = true
        cell.imgCell.image = UIImage.init(named: "dish_tazty_icon")
        
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if((indexPath as NSIndexPath).row >= self.arrFeed.count)
        {
            return
        }
        
        self.selectedDish = (self.arrFeed[(indexPath as NSIndexPath).row] as! String)
        self.performSegue(withIdentifier: "segue_publishvideo", sender: nil)
        
    }
    
    func txtRestaurantNameDidChange() {
        self.arrFeed.removeAll()
        self.loadDishes(self.txtFoodName.text!)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.resignFirstResponder()
        return true
    }

    @IBAction func onNext(_ sender: AnyObject) {
        if self.selectedDish == nil && self.txtFoodName.text!.isEmpty{
            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "You need to pick one dish from list or type new one", titleForButton: "OK", controller: self)
            
            return
        }
        
        self.selectedDish = self.txtFoodName.text
        self.performSegue(withIdentifier: "segue_publishvideo", sender: nil)
    }

    @IBAction func onBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_publishvideo"
        {
            let vc = segue.destination as! PublishVideoController
            vc.foodName = self.selectedDish
            vc.restaurantName = self.restaurant
            vc.thumb = self.thumb
            vc.urlVideo = self.videoURL
            
        }
    }

}
