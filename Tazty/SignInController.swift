//
//  SignInController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SignInController: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    var action: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_showInfo")
        {
            if(action == "Terms")
            {
            let vc = segue.destination as? InfoGenericController
            vc?.textToTitle = "Terms of Service"
            vc?.urlToShow = "http://taztyapp.com/phone/terms-of-service.html"
            }
            else if(action == "Privacy")
            {
                let vc = segue.destination as? InfoGenericController
                vc?.textToTitle = "Privacy Policy"
                vc?.urlToShow = "http://taztyapp.com/phone/privacy-policy.html"
            }
        }
        else if(segue.identifier == "segue_loginEmailSuccess")
        {
            let vc = segue.destination as? HomeController            
            vc?.isAnonymous = false
        }
    }
    
    @IBAction func btnForgotPassword(_ sender: UIButton) {
        
    }

    @IBAction func backToPrevious(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
  
    @IBAction func signInWithEmail(_ sender: UIButton) {
        if(validateInformation())
        {
            let newPost = ["email": txtEmail.text!, "password":txtPassword.text!]
            Alamofire.request(ServicesRouter.loginEmail(parameter: newPost))
                .responseJSON{response in
                    switch response.result{
                    case .success( _):
                        if((response.result.value) != nil)
                        {
                            let login = Login.init(json: JSON(response.result.value!))
                            if (login.code == "200")
                            {
                                Session.sharedInstance.setUserID(login.userId!)
                                Session.sharedInstance.setPhoto(login.photo!)
                                Session.sharedInstance.setUsername(login.fullname!)
                                Session.sharedInstance.setLoginType("3")
                                
                                self.performSegue(withIdentifier: "segue_loginEmailSuccess", sender: sender)
                            }
                            else{
                                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: login.message, titleForButton: "OK", controller: self)
                            }
                        }
                    case .failure(_):
                        CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We have problems with connection try later", titleForButton: "OK", controller: self)
                    }
            }
            
        }
    }

    @IBAction func showTermsOfService(_ sender: UIButton) {
        self.action = "Terms"
        self.performSegue(withIdentifier: "segue_showInfo", sender: sender)
    }
    @IBAction func showPrivacy(_ sender: UIButton) {
        self.action = "Privacy"
        self.performSegue(withIdentifier: "segue_showInfo", sender: sender)
    }
    
    func validateInformation() -> Bool
    {
        if(!(self.txtEmail.text?.isEmpty)! && !(self.txtPassword.text?.isEmpty)!)
        {
            if(CommonFunctions.validateEmail(self.txtEmail.text!))
            {
                return true
            }
            else
            {
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "E-mail address invalid", titleForButton: "OK", controller: self)
            }
        }
        else{
            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Please fill out all fields.", titleForButton: "OK", controller: self)
        }
        
        return false
    }
    
    @IBAction func forgotPassword(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Forgot Password", message: "Please enter email address associated with your account", preferredStyle: .alert)
        
        alert.addTextField(configurationHandler: { (textField) -> Void in
            textField.placeholder = "Email address"
        })
        alert.textFields?.first?.keyboardType = UIKeyboardType.emailAddress
        
        alert.addAction(UIAlertAction(title: "Reset", style: .default, handler: { (action) -> Void in
            let textField = alert.textFields![0] as UITextField
            
            if(CommonFunctions.validateEmail(textField.text))
            {
                
                let newPost = ["email": textField.text!]
                Alamofire.request(ServicesRouter.forgotPassword(parameter: newPost))
                    .responseJSON{response in
                        switch response.result{
                        case .success( _):
                            if((response.result.value) != nil)
                            {
                                let login = Login.init(json: JSON(response.result.value!))
                                if (login.code == "200")
                                {
                                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Check your email to continue the process of recovering password. If the email doesn't appear shortly, please be sure to check your spam", titleForButton: "OK", controller: self)
                                }
                                else{
                                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: login.message, titleForButton: "OK",  controller: self)
                                }
                            }
                        case .failure(_):
                            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We have problems with connection try later",  titleForButton: "OK", controller: self)
                        }
                }
                
            }
            else
            {
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "E-mail address invalid", titleForButton: "OK",  controller: self)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))

        self.present(alert, animated: true, completion: nil)
    }
    
}
