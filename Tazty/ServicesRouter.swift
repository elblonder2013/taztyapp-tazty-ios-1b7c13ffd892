//
//  ServicesRouter.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/13/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import Foundation
import Alamofire

enum ServicesRouter: URLRequestConvertible {
    static let baseURLString = Session.sharedInstance.getAPIURL()
    static let appID = Session.sharedInstance.getAPPID()
    static let contentType = Session.sharedInstance.getContentType()
    
    case getFeed(parameter: Parameters)
    case getFeedNearby(parameter: Parameters)
    case loginEmail(parameter: Parameters)
    case signupEmail(parameter: Parameters)
    case accessWithFB(parameter: Parameters)
    case accessWithTW(parameter: Parameters)
    case forgotPassword(parameter: Parameters)
    case getFeedToMap(parameter: Parameters)
    case setDeviceToken(parameter: Parameters)
    case getProfileInformation(parameter: Parameters)
    case updateFullname(parameter: Parameters)
    case searchPeople(parameter: Parameters)
    case searchRestaurant(parameter: Parameters)
    case searchDishes(parameter: Parameters)
    case getFeedByBar(parameter: Parameters)
    case deletePost(parameter: Parameters)
    case follow(parameter: Parameters)
    case unfollow(parameter: Parameters)
    case like(parameter: Parameters)
    case unlike(parameter: Parameters)
    case report(parameter: Parameters)
    case getFollowers(parameter: Parameters)
    case getFollowings(parameter: Parameters)
    case getFeedByFoodname(parameter: Parameters)
    

        var method: HTTPMethod {
            switch self {
            case .getFeed:
                return .post
            case .getFeedNearby:
                return .post
            case .loginEmail:
                return .post
            case .signupEmail:
                return .post
            case .accessWithFB:
                return .post
            case .accessWithTW:
                return .post
            case .forgotPassword:
                return .post
            case .getFeedToMap:
                return .post
            case .setDeviceToken:
                return .post
            case .getProfileInformation:
                return .post
            case .updateFullname:
                return .post
            case .searchPeople:
                return .post
            case .searchRestaurant:
                return .post
            case .searchDishes:
                return .post
            case .getFeedByBar:
                return .post
            case .deletePost:
                return .post
            case .follow:
                return .post
            case .unfollow:
                return .post
            case .like:
                return .post
            case .unlike:
                return .post
            case .report:
                return .post
            case .getFollowers:
                return .post
            case .getFollowings:
                return .post
            case .getFeedByFoodname:
                return .post
            }
        }
    
        var path : String {
            switch self {
            case .getFeed:
                return "/feed/get_feed"
            case .getFeedNearby:
                return "/feed/get_feed_nearby"
            case .loginEmail:
                return "/user/login"
            case .signupEmail:
                return "/user/signup"
            case .accessWithFB:
                return "/user/login_fb"
            case .accessWithTW:
                return "/user/login_tw"
            case .forgotPassword:
                return "/user/forgot_password"
            case .getFeedToMap:
                return "/feed/get_feed_map"
            case .setDeviceToken:
                return "/user/wakeup"
            case .getProfileInformation:
                return "/user/get_userinfo"
            case .updateFullname:
                return "/user/update_fullname"
            case .searchPeople:
                return "/explore/search_people"
            case .searchDishes:
                return "/explore/search_dishes"
            case .searchRestaurant:
                return "/explore/search_restaurant"
            case .getFeedByBar:
                return "/feed/get_feed_by_bar"
            case .deletePost:
                return "/user/delete_post"
            case .follow:
                return "/user/follow"
            case .unfollow:
                return "/user/unfollow"
            case .like:
                return "/user/like"
            case .unlike:
                return "/user/unlike"
            case .report:
                return "/user/report"
            case . getFollowers:
                return "/explore/followers"
            case .getFollowings:
                return "/explore/followings"
            case .getFeedByFoodname:
                return "/feed/get_feed_foodname"
            }
    }
//            var URL = Foundation.URL(string: ServicesRouter.baseURLString!)!
//            if let relativePath = relativePath {
//                URL = URL.appendingPathComponent(relativePath)
//            }
//            return URL
//        }()
//        
//        let URLRequest = NSMutableURLRequest(url: url)
//        
//        URLRequest.setValue(ServicesRouter.appID, forHTTPHeaderField: "T-APP-ID")
//        URLRequest.setValue(ServicesRouter.contentType, forHTTPHeaderField: "Content-Type")
//        
//        let encoding = Alamofire.ParameterEncoding.json
//        let (encodedRequest, _) = encoding.encode(URLRequest, parameters: params)
//        
//        encodedRequest.httpMethod = method.rawValue
//        
//        return encodedRequest
//    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ServicesRouter.baseURLString?.asURL()
        
        var urlRequest = URLRequest(url: (url?.appendingPathComponent(path))!)
        
        urlRequest.httpMethod = method.rawValue
        urlRequest.addValue(ServicesRouter.appID!, forHTTPHeaderField: "T-APP-ID")
        urlRequest.addValue(ServicesRouter.contentType!, forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .getFeed(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getFeedNearby(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .loginEmail(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .signupEmail(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .accessWithFB(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .accessWithTW(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .forgotPassword(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getFeedToMap(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .setDeviceToken(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getProfileInformation(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .updateFullname(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .searchPeople(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .searchDishes(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .searchRestaurant(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getFeedByBar(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .deletePost(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .follow(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .unfollow(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .like(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .unlike(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .report(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case . getFollowers(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getFollowings(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getFeedByFoodname(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        }
        
        return urlRequest
    }
}
