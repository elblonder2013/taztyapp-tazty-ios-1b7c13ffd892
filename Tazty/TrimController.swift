//
//  TrimController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import GPUImage
import CoreMedia

class TrimController: UIViewController, SAVideoRangeSliderDelegate, GPUImageMovieDelegate {
    
    @IBOutlet weak var vwVideo: UIView!
    @IBOutlet weak var btnBack: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var imgForTick: UIImageView!
    @IBOutlet weak var vwRange: UIView!
    @IBOutlet weak var btnForPlay: UIButton!
    
    var videoSlider : SAVideoRangeSlider?
    
    var picture : GPUImagePicture?
    var movie : GPUImageMovie?
    var movieWriter : GPUImageMovieWriter?
    var imageView : GPUImageView?
    var rotationMode : GPUImageRotationMode?
    fileprivate var filter : GPUImageOutput?
    
    var asset : AVAsset?
    var generator : AVAssetImageGenerator?
    
    var drawing = false
    var exporting = false
    var playing = false
    
    var avPlayer : AVPlayer?
    var videoEdited = false
    var videoURL : URL?
    var finalURL : URL?
    var thumb : UIImage?
    
    
    func setFilter<FilterType:GPUImageOutput>(_ filter:FilterType) where FilterType:GPUImageInput {
        self.filter = filter
    }
    
    override func loadView() {
        super.loadView()
        
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        let myAsset = AVURLAsset.init(url: self.videoURL!, options: nil)
        let videoLength = CMTimeGetSeconds(myAsset.duration)
        
        var frame = self.vwRange.frame
        frame.size.width = frame.size.width * CGFloat(videoLength) / 10.0
        
        self.videoSlider = SAVideoRangeSlider.init(frame: self.vwRange.bounds, videoUrl: self.videoURL)
        self.videoSlider!.delegate = self
        self.videoSlider!.minGap = 4.0
        self.videoSlider!.maxGap = 10.0
        self.videoSlider!.setPopoverBubbleSize(30.0, height: 30.0)
        self.vwRange.addSubview(self.videoSlider!)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.videoEdited = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imageView = GPUImageView.init(frame: self.vwVideo.bounds)
        self.vwVideo.addSubview(self.imageView!)
        
        self.asset = AVURLAsset.init(url: self.videoURL!, options: nil)
        
        let arrTrack = self.asset?.tracks(withMediaType: AVMediaTypeVideo)
        if arrTrack == nil || arrTrack?.count == 0
        {
            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "The selected video is invalid", titleForButton: "OK", controller: self)
            return
        }
        
        self.generator = AVAssetImageGenerator.init(asset: self.asset!)
        self.generator?.appliesPreferredTrackTransform = true
        let assetTrack = arrTrack![0]
        let transform = assetTrack.preferredTransform
        
        if transform.a == 1 && transform.b == 0 && transform.c == 0 && transform.d == 1
        {
            self.rotationMode = .none
        }
        else if transform.a == -1 && transform.b == 0 && transform.c == 0 && transform.d == -1
        {
            self.rotationMode = kGPUImageRotate180
        }
        else if transform.a == 0 && transform.b == -1 && transform.c == 1 && transform.d == 0
        {
            self.rotationMode = kGPUImageRotateLeft
        }
        else if transform.a == 0 && transform.b == 1 && transform.c == -1 && transform.d == 0
        {
            self.rotationMode = kGPUImageRotateRight
        }
        
        self.extractImage(0.1)
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(TrimController.onBack(_:)))
        self.btnBack.isUserInteractionEnabled = true
        self.btnBack.addGestureRecognizer(tapGesture)
    }
    
    func onBack(_ gestureRecognizer : UITapGestureRecognizer)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func videoRange(_ videoRange: SAVideoRangeSlider!, didChangeLeftPosition leftPosition: CGFloat) {
        self.extractImage(leftPosition)
    }
    
    func videoRange(_ videoRange: SAVideoRangeSlider!, didChangeRightPosition rightPosition: CGFloat) {
        self.extractImage(rightPosition)
    }
    
    func videoRange(_ videoRange: SAVideoRangeSlider!, didChangeLeftPosition leftPosition: CGFloat, rightPosition: CGFloat) {
    }
    
    func videoRange(_ videoRange: SAVideoRangeSlider!, didGestureStateEndedLeftPosition leftPosition: CGFloat, rightPosition: CGFloat) {
               self.videoEdited = true
    }
    
    func extractImage(_ position : CGFloat)
    {
        if(self.drawing)
        {
            return
        }
        
        self.drawing = true
        let time = CMTimeMakeWithSeconds(Float64(position), (self.asset?.duration.timescale)!)
        var actualTime = kCMTimeZero
        do
        {
            let image = try self.generator?.copyCGImage(at: time, actualTime: &actualTime)
            let thumb = UIImage.init(cgImage: image!)
            
            self.imgCover.image = thumb
            self.thumb = thumb
            self.drawing = false
        }
        catch( _)
        {
           
        }
    }
    
    func trimVideo()
    {
        let start = self.videoSlider!.leftPosition
        let end = self.videoSlider!.rightPosition
         
        let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("tmp/trim.mp4")
        unlink(path.absoluteString)
        let url = path
        
        _ = try? FileManager.default.removeItem(at: url)
        
        guard let exportSession = AVAssetExportSession(asset: self.asset!, presetName: AVAssetExportPresetMediumQuality) else {return}
        exportSession.outputURL = url
        exportSession.outputFileType = AVFileTypeMPEG4
        
        let startTime = CMTime(seconds: Double(start), preferredTimescale: 1000)
        let endTime = CMTime(seconds: Double(end), preferredTimescale: 1000)
        let timeRange = CMTimeRange(start: startTime, end: endTime)
        
        exportSession.timeRange = timeRange
        exportSession.exportAsynchronously {
            switch exportSession.status
            {
            case .failed, .cancelled:
                DispatchQueue.main.async{
                    self.mergeFailed()
                }
                
                break;
            case .completed:
                DispatchQueue.main.async{
                    self.finalURL = path
                    self.performSegue(withIdentifier: "segue_selectresturant", sender: nil)
//                    if(!self.exporting)
//                    {
//                        self.play()
//                    }
//                    else
//                    {
//                        self.export()
//                    }
                }
                break;
            default:
                DispatchQueue.main.async{
                    if(!self.exporting)
                    {
                        self.play()
                    }
                    else
                    {
                        self.export()
                    }
                }
                break;
            }
        }
        
        
    }
    
    func play()
    {
        if self.avPlayer != nil
        {
            self.avPlayer?.pause()
            self.imgCover.layer.sublayers?.last?.removeFromSuperlayer()
        }
        
        self.imgCover.isHidden = false
        
        var asset : AVURLAsset?
        asset = AVURLAsset.init(url: self.videoURL!, options: nil)
        let item = AVPlayerItem.init(asset: asset!)
        self.avPlayer = AVPlayer.init(playerItem: item)
        self.avPlayer?.actionAtItemEnd = .none
        self.avPlayer?.volume = 0
        
        NotificationCenter.default.addObserver(self, selector: #selector(TrimController.playerItemDidReachEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.avPlayer?.currentItem)
        NotificationCenter.default.addObserver(self, selector: #selector(TrimController.playerItemPlaybackStalled(_:)), name: NSNotification.Name.AVPlayerItemPlaybackStalled, object: self.avPlayer)
        
        let lay = AVPlayerLayer.init(player: self.avPlayer)
        lay.frame = self.imgCover.bounds
        lay.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.imgCover.layer.addSublayer(lay)
        let start = CMTimeMakeWithSeconds(Float64(self.videoSlider!.leftPosition), self.asset!.duration.timescale)
        let duration = CMTimeMakeWithSeconds(Float64(self.videoSlider!.rightPosition - self.videoSlider!.leftPosition), self.asset!.duration.timescale
        )
        self.avPlayer?.seek(to: start)
        self.avPlayer?.volume = 0
        self.avPlayer?.play()
        
        Timer.scheduledTimer(timeInterval: Double(duration.value / Int64(duration.timescale)), target: self, selector: #selector(TrimController.stopPlayingAfterXSecs), userInfo: nil, repeats: false)
        
        var frame = imgForTick.frame
        let thumbnail = ceil(self.videoSlider!.frame.width * 0.05) * 2
        
        frame.origin.x = CGFloat(self.videoSlider!.leftPosition * self.videoSlider!.frame_width / CGFloat(self.videoSlider!.durationSeconds)) + thumbnail
        self.imgForTick.frame = frame
        self.imgForTick.isHidden = true
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(Double(self.videoSlider!.rightPosition - self.videoSlider!.leftPosition))
        frame.origin.x = CGFloat(self.videoSlider!.leftPosition * self.videoSlider!.frame_width / CGFloat(self.videoSlider!.durationSeconds)) - thumbnail
        self.imgForTick.frame = frame
        UIView.commitAnimations()
        
        self.playing = true
        
    }
    
    func stopPlayingAfterXSecs()
    {
        let start = CMTimeMakeWithSeconds(Float64(self.videoSlider!.leftPosition), self.asset!.duration.timescale)
        self.avPlayer?.seek(to: start)
        self.avPlayer?.pause()
        self.btnForPlay.isSelected = false
        self.btnForPlay.isHidden = false
    }
    
    func playerItemDidReachEnd(_ notification : Notification)
    {
        let p = notification.object as! AVPlayerItem
        p.seek(to: kCMTimeZero)
        self.btnForPlay.isHidden = false
    }
    
    func playerItemPlaybackStalled(_ notification : Notification)
    {
        let player = notification.object as! AVPlayer
        player.volume = 0
        player.play()
    }
    
    func stop()
    {
        if self.playing
        {
            self.movie?.cancelProcessing()
            self.imgForTick.layer.removeAllAnimations()
        }
    }
    
    func export()
    {
        
        let videoURLlocal = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("tmp/trim.mp4")
        
        let filename = String.init(format:"tmp/movie_%@.mp4", String(Date().timeIntervalSince1970)
            .replacingOccurrences(of: ".", with: ""))
        let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent(filename)
       // unlink(path.absoluteString)
        let url = path
        
        do
        {
            try FileManager.default.copyItem(atPath: videoURLlocal.absoluteString, toPath: path.absoluteString)
            self.mergeSuccessed(url)
            return
        }
        catch let error as NSError
        {
            print(error.localizedDescription);
        }
        
        let filterURL = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent("tmp/filter.mp4")
        unlink(filterURL.absoluteString)
        
        self.movie?.removeAllTargets()
        self.filter?.removeAllTargets()
        
        self.movie = GPUImageMovie.init(url: videoURLlocal)
        self.movie?.delegate = self
        self.movie?.playAtActualSpeed = false
        
        let tracks = self.asset?.tracks(withMediaType: AVMediaTypeVideo)
        let track = tracks?.last
        var mediaSize = track?.naturalSize
        
        switch self.rotationMode!
        {
        case kGPUImageNoRotation:
            break
        case kGPUImageRotateLeft:
            mediaSize = CGSize(width: (mediaSize?.height)!, height: (mediaSize?.width)!)
            break
        case kGPUImageRotateRight:
            mediaSize = CGSize(width: (mediaSize?.height)!, height: (mediaSize?.width)!)
            break
        case kGPUImageRotate180:
            break
        default:
            break
        }
        
        self.movieWriter = GPUImageMovieWriter.init(movieURL: filterURL, size: mediaSize!)
        self.movieWriter?.shouldPassthroughAudio = false
        self.movieWriter?.setInputRotation(self.rotationMode!, at: 0)
        self.movieWriter?.startRecording()
        self.movie?.startProcessing()
    }
    
    func didCompletePlayingMovie() {
        DispatchQueue.main.async
        {
            if self.playing
            {
                self.imgForTick.layer.removeAllAnimations()
            }
            
            if !self.exporting
            {
                self.imgForTick.isHidden = true
                self.btnForPlay.isSelected = false
            }
            else if (!self.playing)
            {
                self.movieWriter?.finishRecording()
                self.movie?.removeAllTargets()
                self.filter?.removeAllTargets()
                
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(__int64_t(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                    self.merge()
                })
            }
        }
    }
    
    func merge(){
        let composition = AVMutableComposition.init()
        
        let videoAsset = AVURLAsset.init(url: self.videoURL!, options: nil)
        let duration = videoAsset.duration
        let videoAssetTrack = videoAsset.tracks(withMediaType: AVMediaTypeVideo).first
        let videoTrack = composition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        do
        {
            try videoTrack.insertTimeRange(CMTimeRangeMake(kCMTimeZero, duration), of: videoAssetTrack!, at: kCMTimeInvalid)
            
            let filename = String.init(format:"tmp/movie_%@.mp4", String(Date().timeIntervalSince1970)
                .replacingOccurrences(of: ".", with: ""))
            let path = URL(fileURLWithPath: NSHomeDirectory()).appendingPathComponent(filename)
            unlink(path.absoluteString)
            let url = path
            
            let exportSession = AVAssetExportSession.init(asset: composition, presetName: AVAssetExportPresetMediumQuality)
            exportSession?.outputFileType = AVFileTypeMPEG4
            exportSession?.outputURL = url
            
            exportSession!.exportAsynchronously(completionHandler: {
                switch exportSession!.status{
                case .failed, .cancelled:
                    
                    DispatchQueue.main.async{
                        self.mergeFailed()
                    }
                    break
                default:
                    DispatchQueue.main.async{
                        self.mergeSuccessed(url)
                    }
                    
                    break
                }
            })
        }
        catch(_)
        {
            
        }
    }
    
    func mergeSuccessed(_ url : URL)
    {
        self.stop()
        finalURL = url
        
        self.performSegue(withIdentifier: "segue_selectresturant", sender: nil)
        return
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_selectresturant")
        {
            let vc = segue.destination as! ResturantPickerController
            vc.videoURL = finalURL
            vc.thumb = self.thumb
        }
    }
    
    func mergeFailed()
    {
        CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Your video is unavailable for edit", titleForButton: "OK", controller: self)
    }
    
    @IBAction func onPlay(_ sender: AnyObject) {
        self.btnForPlay.isHidden = true
        
        self.btnForPlay.isSelected = true
        self.exporting = false
        self.play()
        self.playing = true
        return
    }
    
    @IBAction func onNext(_ sender: AnyObject) {
        self.exporting = true
        self.stop()
        
        if self.videoEdited == false
        {
            finalURL = videoURL
            self.performSegue(withIdentifier: "segue_selectresturant", sender: nil)
        }
        else
        {
        self.trimVideo()
        }
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
