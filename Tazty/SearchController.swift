//
//  SearchController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import AFNetworking
import SwiftyJSON

class SearchController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnPeople: UIButton!
    @IBOutlet weak var btnDishes: UIButton!
    @IBOutlet weak var btnRestaurants: UIButton!
    @IBOutlet weak var tblSearch: UITableView!
    @IBOutlet weak var vwLoading: UIView!
    @IBOutlet weak var imgLoadingGif: UIImageView!
    
    var isFirstLoad = true
    var arrFeed = [Any]()
    var searchText : String = ""
    var isLoadingFeed = false
    var numberPage : Int = 0
    var isPageRefreshing = false
    var userSelected : People?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loadingGif = UIImage.gifWithName("load")
        self.imgLoadingGif.image = loadingGif
        
        
        self.tblSearch.delegate = self
        self.tblSearch.dataSource = self
        self.searchBar.delegate = self
        self.btnPeople.setTitleColor(Session.sharedInstance.getOrangeTazty(), for: UIControlState())
        
        self.searchBar.setImage(UIImage.init(named: "btn_search-1"), for: .search, state: UIControlState())
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.white
        
        if textFieldInsideSearchBar!.responds(to: #selector(getter: UITextField.attributedPlaceholder)) {
            let attributeDict = [NSForegroundColorAttributeName: UIColor.white]
            textFieldInsideSearchBar!.attributedPlaceholder = NSAttributedString(string: "Search", attributes: attributeDict)
        }
        
        if #available(iOS 9.0, *) {
            (UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])).tintColor = UIColor.white
        } else {
            let cancelButtonAttributes: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
            UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes as? [String : AnyObject], for: UIControlState())
        }
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        view.addGestureRecognizer(edgePan)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatePeopleCell(_:)), name: NSNotification.Name(rawValue: "updatePeopleCell"), object: nil)
        
    }
    
    func updatePeopleCell(_ notification : Notification)
    {
        let peopleInfo = (notification as NSNotification).userInfo!["cellInfo"] as! People
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if (indexPath as NSIndexPath).row < self.arrFeed.count
        {
        self.arrFeed[(indexPath as NSIndexPath).row] = peopleInfo
        }
        self.tblSearch.reloadData()
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loadPeople(_ sender: AnyObject) {
        self.tblSearch.setContentOffset(CGPoint.zero, animated:true)
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.getPeople()
    }
    
    func getPeople()
    {
        self.vwLoading.isHidden = false
        self.btnPeople.isSelected = true
        self.btnDishes.isSelected = false
        self.btnRestaurants.isSelected = false
        self.btnPeople.setTitleColor(Session.sharedInstance.getOrangeTazty(), for: UIControlState())
        self.btnDishes.setTitleColor(Session.sharedInstance.getGrayTazty(), for: UIControlState())
        self.btnRestaurants.setTitleColor(Session.sharedInstance.getGrayTazty(), for: UIControlState())
        
        let newPost = ["user_id": Session.sharedInstance.getUserID()!, "keyword":self.searchText, "page": "\(self.numberPage)", "rows": "25"]
        Alamofire.request(ServicesRouter.searchPeople(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        if !self.isPageRefreshing
                        {
                            self.arrFeed.removeAll()
                        }
                        
                        let jsonResult = JSON(response.result.value!)
                        for item in jsonResult.array!
                        {
                            let people = People.init(json: item)
   
                            self.arrFeed.append(people)
                            
                        }
                        
                        self.tblSearch.reloadData()
                        self.isPageRefreshing = false
                    }
                case .failure( let error):
                    print(error)
                }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.btnPeople.isSelected
        {
            if self.userSelected != nil{
                if UserDefaults.standard.object(forKey: "userUpdated") != nil{
                    let values = UserDefaults.standard.object(forKey: "userUpdated") as! [String:AnyObject]
                    
                    let user = values["user"] as! String
                    let youFollow = values["isFollowing"]!.boolValue!
                    if user == self.userSelected?.userId
                    {
                        for i in 0...(self.arrFeed.count - 1)
                        {
                            let newPeople = self.arrFeed[i] as! People
                            
                            if newPeople.userId == self.userSelected?.userId
                            {
                                newPeople.isFollowing = youFollow ? "1" : "0"
                                self.arrFeed[i] = newPeople
                                
                            }
                        }
                        self.tblSearch.reloadData()
                    }
                }
            }
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if(self.isFirstLoad)
        {
            self.vwLoading.isHidden = false
            self.btnPeople.isSelected = true
            let newPost = ["user_id": Session.sharedInstance.getUserID()!, "keyword":"", "page": "\(self.numberPage)", "rows": "25"]
            Alamofire.request(ServicesRouter.searchPeople(parameter: newPost))
                .responseJSON{response in
                    switch response.result{
                    case .success( _):
                        if((response.result.value) != nil)
                        {
                            self.arrFeed.removeAll()
                            
                            let jsonResult = JSON(response.result.value!)
                            for item in jsonResult.array!
                            {
                                let people = People.init(json: item)
                                self.arrFeed.append(people)
                            }
                            
                            self.tblSearch.reloadData()
                            
                        }
                    case .failure( let error):
                        print(error)
                    }
            }
            
            self.isFirstLoad = false
        }
    }
    
    @IBAction func loadDishes(_ sender: AnyObject) {
        self.tblSearch.setContentOffset(CGPoint.zero, animated:true)
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.getDishes()
    }
    
    func getDishes()
    {
        self.vwLoading.isHidden = false
        self.btnDishes.isSelected = true
        self.btnPeople.isSelected = false
        self.btnRestaurants.isSelected = false
        self.btnPeople.setTitleColor(Session.sharedInstance.getGrayTazty(), for: UIControlState())
        self.btnDishes.setTitleColor(Session.sharedInstance.getOrangeTazty(), for: UIControlState())
        self.btnRestaurants.setTitleColor(Session.sharedInstance.getGrayTazty(), for: UIControlState())
        
        let newPost = ["foodname": self.searchText]
        Alamofire.request(ServicesRouter.searchDishes(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {

                            self.arrFeed.removeAll()
                        
                        let jsonResult = JSON(response.result.value!)
                        for item in jsonResult.array!
                        {
                            let foodname = item.stringValue
                            if(!foodname.isEmpty)
                            {
                                self.arrFeed.append(foodname)
                            }
                        }
                        
                        self.tblSearch.reloadData()
                    }
                case .failure( let error):
                    print(error)
                }
        }
        
    }
    
    @IBAction func loadRestaurants(_ sender: AnyObject) {
        self.tblSearch.setContentOffset(CGPoint.zero, animated:true)
        self.searchBar.text = ""
        self.searchBar.resignFirstResponder()
        self.getRestaurants()
    }
    
    func getRestaurants()
    {
        self.vwLoading.isHidden = false
        self.btnRestaurants.isSelected = true
        self.btnPeople.isSelected = false
        self.btnDishes.isSelected = false
        self.btnPeople.setTitleColor(Session.sharedInstance.getGrayTazty(), for: UIControlState())
        self.btnDishes.setTitleColor(Session.sharedInstance.getGrayTazty(), for: UIControlState())
        self.btnRestaurants.setTitleColor(Session.sharedInstance.getOrangeTazty(), for: UIControlState())
        
        Session.sharedInstance.setLatitude(Session.sharedInstance.getLatitude() == nil ? "0" : Session.sharedInstance.getLatitude()!)
        Session.sharedInstance.setLongitude(Session.sharedInstance.getLongitude() == nil ? "0" : Session.sharedInstance.getLongitude()!)
        
        let newPost = ["barname": self.searchText, "latitude": Session.sharedInstance.getLatitude()! , "longitude": Session.sharedInstance.getLongitude()!, "page": "\(self.numberPage)", "rows": "25"]
        Alamofire.request(ServicesRouter.searchRestaurant(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        if !self.isPageRefreshing
                        {
                            self.arrFeed.removeAll()
                        }
                        
                        let jsonResult = JSON(response.result.value!)
                        for item in jsonResult.array!
                        {
                            let restaurant = Restaurant.init(json: item)
                            self.arrFeed.append(restaurant)
                        }
                        
                        self.tblSearch.reloadData()
                        self.isPageRefreshing = false
                    }
                case .failure( let error):
                    print(error)
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "ExploreCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ExploreCell
        
        if (indexPath as NSIndexPath).row < self.arrFeed.count
        {
        
        if self.btnPeople.isSelected
        {
            
            if self.arrFeed[(indexPath as NSIndexPath).row] is People
            {
                
                let itemPeople = self.arrFeed[(indexPath as NSIndexPath).row] as! People
                
                cell.isPeople = true
                cell.indexPath = indexPath
                cell.item = itemPeople
                cell.viewController = self
                cell.lblDistance.isHidden = true
                cell.lblNumberDishes.isHidden = true
                cell.lblCell.text = itemPeople.fullname
                cell.imgFollow.isHidden = false
                cell.imgFollow.image = itemPeople.isFollowing == "0" ? UIImage(named:"masorange") : UIImage(named:"palomitaorange")
                if itemPeople.userId == Session.sharedInstance.getUserID()
                {
                    cell.imgFollow.isHidden = true
                }
                cell.imgCell.image = UIImage.init(named: "profile_img_default")
                let url = URL(string: itemPeople.photo!)
                if url != nil
                {
                cell.imgCell.setImageWith(url!)
                }
                cell.selectionStyle = .none
                cell.configureCell()
            }
            
        }
        else if self.btnDishes.isSelected
        {
            if self.arrFeed[(indexPath as NSIndexPath).row] is String
            {
                let itemDish = self.arrFeed[(indexPath as NSIndexPath).row] as! String
                
                cell.lblDistance.isHidden = true
                cell.lblNumberDishes.isHidden = true
                cell.lblCell.text = itemDish
                cell.imgFollow.isHidden = true
                cell.imgCell.image = UIImage.init(named: "dish_tazty_icon")
                cell.lblCell.isUserInteractionEnabled = false
                cell.imgCell.isUserInteractionEnabled = false
            }
        }
        else if self.btnRestaurants.isSelected
        {
            if self.arrFeed[(indexPath as NSIndexPath).row] is Restaurant
            {
                let itemRestaurant = self.arrFeed[(indexPath as NSIndexPath).row] as! Restaurant
                
                cell.lblDistance.isHidden = false
                cell.lblDistance.text = itemRestaurant.distance! + " mi"
                cell.lblCell.text = itemRestaurant.barname
                cell.imgFollow.isHidden = true
                cell.lblNumberDishes.isHidden = false
                cell.lblNumberDishes.text = itemRestaurant.videos! + " dishes"
                cell.imgCell.image = UIImage.init(named: "locationRestaurante")
                cell.lblCell.isUserInteractionEnabled = false
                cell.imgCell.isUserInteractionEnabled = false
            }
        }
        
        cell.selectionStyle = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if (indexPath as NSIndexPath).row == (tableView.indexPathsForVisibleRows?.last as NSIndexPath?)?.row
        {
            self.vwLoading.isHidden = true;
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.btnPeople.isSelected
        {
            
        }
        else if self.btnDishes.isSelected
        {
            self.performSegue(withIdentifier: "segue_search_dish_feed", sender: indexPath)
        }
        else if self.btnRestaurants.isSelected
        {
            self.performSegue(withIdentifier: "segue_search_restaurant", sender: indexPath)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if(self.btnRestaurants.isSelected)
        {
            self.searchText = searchText
            self.getRestaurants()
        }
        else if(self.btnDishes.isSelected)
        {
            self.searchText = searchText
            self.getDishes()
        }
        else if(self.btnPeople.isSelected)
        {
            self.searchText = searchText
            self.getPeople()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblSearch.contentOffset.y >= self.tblSearch.contentSize.height - self.tblSearch.bounds.size.height
        {
            if self.isPageRefreshing == false
            {
                self.isPageRefreshing = true
                self.numberPage += 25
                if self.btnPeople.isSelected
                {
                    self.getPeople()
                }
                else if self.btnDishes.isSelected
                {
                    self.getDishes()
                }
                else if self.btnRestaurants.isSelected
                {
                    self.getRestaurants()
                }
                self.tblSearch.reloadData()
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_search_restaurant"
        {
            let index = sender as! IndexPath
            
            let itemRestaurant = self.arrFeed[(index as NSIndexPath).row] as! Restaurant
            let itemCell = self.tblSearch.cellForRow(at: index) as! ExploreCell
            
            let vc = segue.destination as? RestaurantController
            vc?.countVideos = itemCell.lblNumberDishes.text
            vc?.restaurantName = itemCell.lblCell.text
            vc?.distance = itemRestaurant.distance
            
            let barlocation = CLLocationCoordinate2D.init(latitude: Double(itemRestaurant.latitude!)!, longitude: Double(itemRestaurant.longitude!)!)
            
            vc?.restaurantLocation = barlocation
        }
        else if segue.identifier == "segue_search_people"
        {
            let index = sender as! People
            self.userSelected = index
            let profileController = segue.destination as! ProfileController
            profileController.feed_user = index.userId
            
        }
        else if segue.identifier == "segue_search_dish_feed"
        {
            let index = sender as! IndexPath
            
            let vc = segue.destination as! DishController
            vc.foodname = (self.arrFeed[(index as NSIndexPath).row] as! String)
        }
    }
    
    
}
