//
//  ExploreCell.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/14/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire

class ExploreCell: UITableViewCell {
    @IBOutlet weak var imgCell: UIImageView!
    @IBOutlet weak var lblCell: UILabel!
    @IBOutlet weak var imgFollow: UIImageView!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblNumberDishes: UILabel!
    
    var viewController : UIViewController?
    var indexPath : IndexPath?
    var item : AnyObject?
    var isPeople : Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell()
    {
        if (self.isPeople != nil)
        {
            if self.isPeople!
            {
                self.imgCell.isUserInteractionEnabled = true
                let tap = UITapGestureRecognizer.init(target: self, action: #selector(ExploreCell.goToProfile(_:)))
                self.imgCell.addGestureRecognizer(tap)
                
                self.lblCell.isUserInteractionEnabled = true
                let tap2 = UITapGestureRecognizer.init(target: self, action: #selector(ExploreCell.goToProfile(_:)))
                self.lblCell.addGestureRecognizer(tap2)
                
                self.imgFollow.isUserInteractionEnabled = true
                let tap3 = UITapGestureRecognizer.init(target: self, action: #selector(ExploreCell.follow(_:)))
                self.imgFollow.addGestureRecognizer(tap3)
            }
        }

    }
    
    func follow(_ gestureRecognizer : UITapGestureRecognizer)
    {
        let people = item as! People
        
        if people.isFollowing == "1"
        {
            let newPost : [String : AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "userToFollow" : people.userId! as AnyObject]
            Alamofire.request(ServicesRouter.unfollow(parameter: newPost))
                .responseJSON{ response in
                    switch response.result{
                    case .success( _):
                        self.imgFollow.image = UIImage(named:"masorange")
                        people.isFollowing = "0"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updatePeopleCell"), object: nil, userInfo: ["cellInfo" : people, "indexPath" : self.indexPath!])
                        break
                    case .failure( _):
                      
                        break
                    }
            }
        }
        else if people.isFollowing == "0"
        {
            let newPost : [String : AnyObject] = ["userId" : Session.sharedInstance.getUserID()! as AnyObject, "userToFollow" : people.userId! as AnyObject]
            Alamofire.request(ServicesRouter.follow(parameter: newPost))
                .responseJSON{ response in
                    switch response.result{
                    case .success( _):
                        self.imgFollow.image = UIImage(named:"palomitaorange")
                        people.isFollowing = "1"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updatePeopleCell"), object: nil, userInfo: ["cellInfo" : people, "indexPath" : self.indexPath!])
                        break
                    case .failure( _):
                        
                        break
                    }
            }
        }
    }
    
    func goToProfile(_ gestureRecognizer : UITapGestureRecognizer)
    {
        if self.viewController is FollowsController
        {
        self.viewController?.performSegue(withIdentifier: "segue_follow_profile", sender: item as! People)
        }
        else if self.viewController is SearchController
        {
            self.viewController?.performSegue(withIdentifier: "segue_search_people", sender: item)
        }
    }
    
    override func layoutSubviews() {
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
