//
//  ViewController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 2/18/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import TwitterKit
import FBSDKLoginKit
import Alamofire
import SwiftyJSON

class OpenController: UIViewController {
    
    var isAnonymous = false
    
    @IBOutlet weak var imgLoding: UIImageView!
    @IBOutlet weak var vwLoading: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let loadingGif = UIImage.gifWithName("load")
        self.imgLoding.image = loadingGif
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnFBLoginPressed(_ sender: AnyObject) {
        self.vwLoading.isHidden = false
        self.requestReadPermissions()
    }
    
//    func requestPublishPermission()
//    {
//        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//        
//        fbLoginManager.logIn(withPublishPermissions: ["publish_actions"], from: nil, handler: { (response: FBSDKLoginManagerLoginResult!, error: NSError!) in
//            if (error != nil){
//                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "An error ocurren with Facebook connection, try later", titleForButton: "OK",  controller: self)
//            }
//            else if(response.isCancelled)
//            {
//                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Facebook login cancelled by user", titleForButton: "OK",  controller: self)
//            }
//            else if(response.grantedPermissions.contains("publish_actions"))
//            {
//                /*
//                Make an Action with publish permissions
//                */
//            }
//        })
//    }
    
    func requestReadPermissions()
    {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.loginBehavior = .native
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: nil) { (response, error) in
            if (error != nil){
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "An error ocurren with Facebook connection, try later", titleForButton: "OK",  controller: self)
                self.vwLoading.isHidden = true
            }
            else if(response?.isCancelled)!
            {
                self.vwLoading.isHidden = true
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Facebook login cancelled by user", titleForButton: "OK",  controller: self)
            }
            else if(response?.grantedPermissions.contains("email"))!
            {
                self.vwLoading.isHidden = true
                self.returnUserData()
            }
            else
            {
                self.vwLoading.isHidden = true
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "Email permission required by Tazty to register has been disabled by user, please enter to your FB account and set permission from settings and retry", titleForButton: "OK",  controller: self)
            }
        }
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"first_name, last_name, picture.type(large), email"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                self.vwLoading.isHidden = true
               CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "An error ocurren with Facebook connection, try later", titleForButton: "OK",  controller: self)
            }
            else
            {
                self.vwLoading.isHidden = true
                self.allDataFound(result as AnyObject)
            }
        })
    }
    
    func allDataFound(_ result : AnyObject)
    {
        let jsonresult = JSON(result)
        var firstName = ""
        var id = ""
        var picture = ""
        
        if jsonresult["first_name"] != JSON.null{
                firstName = jsonresult["first_name"].stringValue
        }
        else
        {
            firstName = ""
        }
        
        if jsonresult["id"] != JSON.null{
                id = jsonresult["id"].stringValue
        }
        else
        {
            id = "123456789"
        }
        
        if jsonresult["picture"]["data"]["url"] != JSON.null
        {
            picture = jsonresult["picture"]["data"]["url"].stringValue
        }
        else
        {
            picture = ""
        }
        
        if jsonresult["email"] != JSON.null
        {
            if jsonresult["email"].stringValue != ""
            {
                self.sendFBInformation(jsonresult["email"].stringValue, firstName: firstName, fb_id: id, picture: picture)
            }
            else
            {
                let alert = UIAlertController(title: "Missing information", message: "Please enter a valid email address to confirm your register", preferredStyle: .alert)
                
                alert.addTextField(configurationHandler: { (textField) -> Void in
                    textField.placeholder = "Email address"
                })
                alert.textFields?.first?.keyboardType = UIKeyboardType.emailAddress
                
                alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { (action) -> Void in
                    let textField = alert.textFields![0] as UITextField
                    
                    if(CommonFunctions.validateEmail(textField.text))
                    {
                        //////////////////////////////////
                        self.sendFBInformation(textField.text!, firstName: firstName, fb_id: id, picture: picture)
                        
                        //////////////////////////////////
                    }
                    else
                    {
                        CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "E-mail address invalid", titleForButton: "OK",  controller: self)
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We need a valid email address to end your register please try again", titleForButton: "OK", controller: self)
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        else
        {
            let alert = UIAlertController(title: "Missing information", message: "Please enter a valid email address to confirm your register", preferredStyle: .alert)
            
            alert.addTextField(configurationHandler: { (textField) -> Void in
                textField.placeholder = "Email address"
            })
            alert.textFields?.first?.keyboardType = UIKeyboardType.emailAddress
            
            alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { (action) -> Void in
                let textField = alert.textFields![0] as UITextField
                
                if(CommonFunctions.validateEmail(textField.text))
                {
                    //////////////////////////////////
                    self.sendFBInformation(textField.text!, firstName: firstName, fb_id: id, picture: picture)
                    
                    //////////////////////////////////
                }
                else
                {
                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "E-mail address invalid", titleForButton: "OK",  controller: self)
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) -> Void in
                CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We need a valid email address to end your register please try again", titleForButton: "OK", controller: self)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func sendFBInformation(_ email : String, firstName : String, fb_id : String, picture : String)
    {
        let newPost = ["email": email, "fullname":firstName, "fb_id": fb_id, "photo": picture]
        Alamofire.request(ServicesRouter.accessWithFB(parameter: newPost))
            .responseJSON{response in
                switch response.result{
                case .success( _):
                    if((response.result.value) != nil)
                    {
                        let login = Login.init(json: JSON(response.result.value!))
                        if (login.code == "200")
                        {
                            Session.sharedInstance.setUserID(login.userId!)
                            Session.sharedInstance.setPhoto(login.photo!)
                            Session.sharedInstance.setUsername(login.fullname!)
                            Session.sharedInstance.setLoginType("1")
                            
                            
                            self.isAnonymous = false
                            self.vwLoading.isHidden = true
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
                                
                                self.performSegue(withIdentifier: "segue_loginsuccess", sender: nil)
                            }
                            
                        }
                        else{
                            self.vwLoading.isHidden = true
                            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: login.message, titleForButton: "OK",  controller: self)
                        }
                    }
                case .failure(_):
                    self.vwLoading.isHidden = true
                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We have problems with connection try later", titleForButton: "OK",  controller: self)
                }
        }
    }
    
    @IBAction func btnLoginTW(_ sender: UIButton) {
        self.vwLoading.isHidden = false
        Twitter.sharedInstance().logIn(withMethods: [.webBased]) { session, error in
            if (session != nil) {
                let client = TWTRAPIClient.withCurrentUser()
                let request = client.urlRequest(withMethod: "GET",
                    url: "https://api.twitter.com/1.1/account/verify_credentials.json",
                    parameters: ["include_email": "true", "skip_status": "true"],
                    error: nil)
                
                client.sendTwitterRequest(request) { response, data, connectionError in
                    if(connectionError == nil)
                    {
                        var  json: NSDictionary!
                        do {
                            json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                            } catch let error as NSError {
                                print(error)
                                }
                        
                        
                        let newPost = ["email": json.value(forKey: "email") as! String, "fullname":json.value(forKey: "name") as! String, "tw_id": json.value(forKey: "id_str") as! String, "photo": json.value(forKey: "profile_image_url") as! String]
                        Alamofire.request(ServicesRouter.accessWithTW(parameter: newPost))
                            .responseJSON{response in
                                switch response.result{
                                case .success( _):
                                    if((response.result.value) != nil)
                                    {
                                        let login = Login.init(json: JSON(response.result.value!))
                                        if (login.code == "200")
                                        {
                                            Session.sharedInstance.setUserID(login.userId!)
                                            Session.sharedInstance.setPhoto(login.photo!)
                                            Session.sharedInstance.setUsername(login.fullname!)
                                            Session.sharedInstance.setLoginType("2")
                                            
                                            
                                            self.isAnonymous = false
                                            self.vwLoading.isHidden = true
                                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
                                                
                                                self.performSegue(withIdentifier: "segue_loginsuccess", sender: nil)
                                            }
                                            
                                        }
                                        else{
                                            self.vwLoading.isHidden = true
                                            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: login.message, titleForButton: "OK",  controller: self)
                                        }
                                    }
                                case .failure(_):
                                    self.vwLoading.isHidden = true
                                    CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We have problems with connection try later", titleForButton: "OK",  controller: self)
                                }
                        }
                    }
                }
                
            } else {
                self.vwLoading.isHidden = true
              
            }
        }
    }
    
    @IBAction func btnLoginWithEmail(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segue_loginWithEmail", sender: sender)
    }
    
    @IBAction func signInAnonymous(_ sender: UIButton) {
        self.isAnonymous = true
        self.performSegue(withIdentifier: "segue_loginsuccess", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "segue_loginsuccess")
        {
            let vc = segue.destination as? HomeController
            
            vc?.isAnonymous = self.isAnonymous
        }
    }
    
}

