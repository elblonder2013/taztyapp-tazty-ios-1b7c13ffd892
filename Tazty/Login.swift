//
//  Login.swift
//
//  Created by Hugo Hernandez on 6/15/16
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

open class Login: NSObject, NSCoding  {

    // MARK: Declaration for string constants to be used to decode and also serialize.
	internal let kLoginFullnameKey: String = "fullname"
	internal let kLoginCodeKey: String = "code"
	internal let kLoginUserIdKey: String = "user_id"
	internal let kLoginPhotoKey: String = "photo"
	internal let kLoginMessageKey: String = "message"


    // MARK: Properties
	open var fullname: String?
	open var code: String?
	open var userId: String?
	open var photo: String?
	open var message: String?


    // MARK: SwiftyJSON Initalizers
    /**
    Initates the class based on the object
    - parameter object: The object of either Dictionary or Array kind that was passed.
    - returns: An initalized instance of the class.
    */
    convenience public init(object: AnyObject) {
        self.init(json: JSON(object))
    }

    /**
    Initates the class based on the JSON that was passed.
    - parameter json: JSON object from SwiftyJSON.
    - returns: An initalized instance of the class.
    */
    public init(json: JSON) {
		fullname = json[kLoginFullnameKey].string
		code = json[kLoginCodeKey].string
		userId = json[kLoginUserIdKey].string
		photo = json[kLoginPhotoKey].string
		message = json[kLoginMessageKey].string

    }

    /**
    Generates description of the object in the form of a NSDictionary.
    - returns: A Key value pair containing all valid values in the object.
    */
    open func dictionaryRepresentation() -> [String : AnyObject ] {

        var dictionary: [String : AnyObject ] = [ : ]
		if fullname != nil {
			dictionary.updateValue(fullname! as AnyObject, forKey: kLoginFullnameKey)
		}
		if code != nil {
			dictionary.updateValue(code! as AnyObject, forKey: kLoginCodeKey)
		}
		if userId != nil {
			dictionary.updateValue(userId! as AnyObject, forKey: kLoginUserIdKey)
		}
		if photo != nil {
			dictionary.updateValue(photo! as AnyObject, forKey: kLoginPhotoKey)
		}
		if message != nil {
			dictionary.updateValue(message! as AnyObject, forKey: kLoginMessageKey)
		}

        return dictionary
    }

    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
		self.fullname = aDecoder.decodeObject(forKey: kLoginFullnameKey) as? String
		self.code = aDecoder.decodeObject(forKey: kLoginCodeKey) as? String
		self.userId = aDecoder.decodeObject(forKey: kLoginUserIdKey) as? String
		self.photo = aDecoder.decodeObject(forKey: kLoginPhotoKey) as? String
		self.message = aDecoder.decodeObject(forKey: kLoginMessageKey) as? String

    }

    open func encode(with aCoder: NSCoder) {
		aCoder.encode(fullname, forKey: kLoginFullnameKey)
		aCoder.encode(code, forKey: kLoginCodeKey)
		aCoder.encode(userId, forKey: kLoginUserIdKey)
		aCoder.encode(photo, forKey: kLoginPhotoKey)
		aCoder.encode(message, forKey: kLoginMessageKey)

    }

}
