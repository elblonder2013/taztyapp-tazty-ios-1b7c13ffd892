//
//  VideoManager.swift
//  Tazty
//
//  Created by Victor Hernandez on 06/09/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import Foundation
import Alamofire

class VideoManager
{
    class func downloadVideoWithURL(_ url : String){
        
        // Si ya descargamos el video nos retachamos y no lo volvemos a descargar
        if UserDefaults.standard.bool(forKey: url) == true {
            //Esto significa que ya existe el video
            let fileManager = FileManager.default
            let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let fileName = URL(string: url)?.lastPathComponent
            let filePath = directoryURL.appendingPathComponent(fileName!)
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "cKNewVideoLoaded"), object: [url, filePath])
            return
        }
        // Si estamos descargando el video no volver a intentar descargarlo (Que guapo :V)
        if UserDefaults.standard.bool(forKey: "temp" + url) == true { return }
        
        //Para saber que estamos descargando
        self.userDefaultBoolForKey(true, key: "temp" + url)
        
        download(url) { (temporaryURL, response) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions) in
            //Terminamos de descargar
            self.userDefaultBoolForKey(false, key: "temp" + url)
            
            //Para avisar que ya esta descargado
            self.userDefaultBoolForKey(true, key: url)
            
            //***************************************************************************
            
            let fileManager = FileManager.default
            let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let pathComponent = response.suggestedFilename
            
            let videoData : NSArray = [url , directoryURL.appendingPathComponent(pathComponent!)]
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "cKNewVideoLoaded"), object: videoData)
            
            return (directoryURL.appendingPathComponent(pathComponent!), [.removePreviousFile, .createIntermediateDirectories])
        }
    }
    
    class func userDefaultBoolForKey(_ value: Bool, key:String){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
}
