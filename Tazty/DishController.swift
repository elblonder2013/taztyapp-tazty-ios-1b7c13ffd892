//
//  DishController.swift
//  Tazty
//
//  Created by Victor Hernandez on 20/07/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation
import SwiftyJSON

class DishController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblFeed: UITableView!
    
    var foodname : String?
    var arrFeed = [Feed]()
    var numberPage : Int = 0
    var isPageRefreshing = false
    var isFirstLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lblTitle.text = self.foodname
        
        self.tblFeed.dataSource = self
        self.tblFeed.delegate = self
        
        // Return with swipe
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        NotificationCenter.default.addObserver(self, selector: #selector(deleteCellHome(_:)), name: NSNotification.Name(rawValue: "deleteCellHome"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateCellHome(_:)), name: NSNotification.Name(rawValue: "updateCellHome"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.loadFeed()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func updateCellHome(_ notification: Notification)
    {
        let feedInfo = (notification as NSNotification).userInfo!["cellInfo"] as! Feed
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if ((indexPath as NSIndexPath).row < self.arrFeed.count)
        {
        self.arrFeed[(indexPath as NSIndexPath).row] = feedInfo
        
        self.tblFeed.reloadData()
        }
    }
    
    func deleteCellHome(_ notification : Notification)
    {
        let indexPath = (notification as NSNotification).userInfo!["indexPath"] as! IndexPath
        
        if indexPath.row < self.arrFeed.count
        {
        self.arrFeed.remove(at: indexPath.row)
        
        self.tblFeed.beginUpdates()
        self.tblFeed.deleteRows(at: [indexPath], with: .fade)
        self.tblFeed.endUpdates()
        }
    }
    
    func loadFeed()
    {
        
        self.foodname = self.foodname != nil ? self.foodname : ""
        
        if Session.sharedInstance.getUserID() != nil
        {
            if !(Session.sharedInstance.getUserID()!.isEmpty)
            {
                Session.sharedInstance.setLatitude(Session.sharedInstance.getLatitude() != nil ? Session.sharedInstance.getLatitude()! : "0")
                Session.sharedInstance.setLongitude(Session.sharedInstance.getLongitude() != nil ? Session.sharedInstance.getLongitude()! : "0")
                
                let newPost : [String : AnyObject] = ["userId": Session.sharedInstance.getUserID()! as AnyObject,"foodname" : self.foodname! as AnyObject, "latitude" : Session.sharedInstance.getLatitude()! as AnyObject, "longitude" : Session.sharedInstance.getLongitude()! as AnyObject, "page": "\(self.numberPage)" as AnyObject, "rows": "25" as AnyObject]
                
                Alamofire.request(ServicesRouter.getFeedByFoodname(parameter: newPost))
                    .responseJSON{response in
                        switch response.result{
                        case .success( _):
                            if((response.result.value) != nil)
                            {
                                let jsonResult = JSON(response.result.value!)
                
                                for itemFeed in jsonResult["data"].array!{
                                    let feedItem = Feed.init(json: itemFeed)
                                    self.arrFeed.append(feedItem)
                                    
                                }
                                self.tblFeed.reloadData()
                                
                                
                                
                                self.isPageRefreshing = false
                            }
                        case .failure(let error):
                            CommonFunctions.showAlertWithTitle("Tazty", messageForAlert: "We can't retrieve information, try again later", titleForButton: "OK", controller: self)
                            print(error)
                        }
                }
            }
        }
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 430
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrFeed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "FeedCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! FeedCell
        
        let feed = self.arrFeed[(indexPath as NSIndexPath).row]
        
        let origin = CLLocationCoordinate2D.init(latitude: Double(feed.mylatitude!)!, longitude: Double(feed.mylongitude!)!)
        
        let userLocation = CLLocationCoordinate2D.init(latitude: Double(Session.sharedInstance.getLatitude()!)!, longitude: Double(Session.sharedInstance.getLongitude()!)!)
        
        let distance = String(format: "%.2f", CommonFunctions.distanceMiles(origin, destination: userLocation))
        
        cell.indexPath = indexPath
        cell.lblFoodName.text                   = feed.foodname
        cell.lblRestaurantName.text             = feed.barname! + " - " + distance + "mi"
        cell.lblLikeCounter.titleLabel?.text    = feed.likesNum
        cell.lblUser.text                       = feed.fullname
        cell.lblTimeElapsed.text                = CommonFunctions.timeElapsed(feed.postDate!)
        
        cell.lblLikeCounter.setTitle(feed.likesNum, for: UIControlState())
        cell.lblLikeCounter.setTitleColor(feed.likeState == "0" ? Session.sharedInstance.getOrangeTazty() : UIColor.white, for: UIControlState())
        cell.lblLikeCounter.backgroundColor     = feed.likeState == "0" ? UIColor.white : Session.sharedInstance.getOrangeTazty()
        
        cell.likeButton.setImage(feed.likeState == "0" ? UIImage(named:"Tastys_empty") : UIImage(named:"Tastys_Fill"), for: UIControlState())
        
        cell.item = feed
        cell.viewController = self
        
        //Imagen
        cell.imgUser.image = UIImage(named: "profile_img_default")
        let url = URL(string: feed.photo!)
        if url != nil
        {
        cell.imgUser.setImageWith(url!)
        }
        
        //Video
        let vURLString = "http://38.100.119.14/fballr/uploads/" + feed.video!
        cell.currentURL = vURLString
        
        VideoManager.downloadVideoWithURL(vURLString)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        if (indexPath as NSIndexPath).row == (tableView.indexPathsForVisibleRows?.last as NSIndexPath?)?.row
        {
            if(self.isFirstLoad)
            {
                
                self.goTableTop()
                self.isFirstLoad = false
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.arrFeed.count>=25
        {
        if self.tblFeed.contentOffset.y >= self.tblFeed.contentSize.height - self.tblFeed.bounds.size.height
        {
            if self.isPageRefreshing == false
            {
                self.isPageRefreshing = true
                self.numberPage += 25
                self.loadFeed()
                self.tblFeed.reloadData()
            }
        }
        }
    }
    
    func goTableTop()
    {
        self.tblFeed.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let feedCell : FeedCell = cell as! FeedCell
        feedCell.stopAndRemoveCurrentVideo()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_dish_to_restaurant"
        {
            if sender is Feed
            {
                let  itemFeed = sender as! Feed
                let vc = segue.destination as? RestaurantController
                vc?.countVideos = "videos"
                vc?.restaurantName = itemFeed.barname
                vc?.distance = "mi"
                
                let barlocation = CLLocationCoordinate2D.init(latitude: Double(itemFeed.mylatitude!)!, longitude: Double(itemFeed.mylongitude!)!)
                
                vc?.restaurantLocation = barlocation
            }
        }
        else if segue.identifier == "segue_dish_to_profile"
        {
            let profileController = segue.destination as! ProfileController
            if sender is Feed
            {
                let  itemFeed = sender as! Feed
                profileController.feed_user = itemFeed.userId
            }
        }
    }
}
