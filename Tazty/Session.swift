//
//  Session.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/18/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import Foundation
import MapKit

class Session{
    static let sharedInstance = Session()
    
    fileprivate init(){}
    
    func getUsername() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let username = prefs.string(forKey: "username"){
            return username
        }
        else{
            return nil
        }
    }
    
    func setUsername(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "username")
        prefs.synchronize()
    }
    
    func getLatitude() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let latitude = prefs.string(forKey: "latitude"){
            return latitude
        }
        else{
            return nil
        }
    }
    
    func setLatitude(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "latitude")
        prefs.synchronize()
    }
    
    func getLongitude() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let longitude = prefs.string(forKey: "longitude"){
            return longitude
        }
        else{
            return nil
        }
    }
    
    func setLongitude(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "longitude")
        prefs.synchronize()
    }
    
    func getUserID() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let user_id = prefs.string(forKey: "user_id"){
            return user_id
        }
        else{
            return nil
        }
    }
    
    func setUserID(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "user_id")
        prefs.synchronize()
    }
    
    func getPhoto() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let photo = prefs.string(forKey: "photo"){
            return photo
        }
        else{
            return nil
        }
    }
    
    func setPhoto(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "photo")
        prefs.synchronize()
    }
    
    func getLoginType() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let loginType = prefs.string(forKey: "loginType"){
            return loginType
        }
        else{
            return nil
        }
    }
    
    func setLoginType(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "loginType")
        prefs.synchronize()
    }
    
    func getShortcut() -> String?
    {
        let prefs = UserDefaults.standard
        
        if let shortcut = prefs.string(forKey: "shortcut"){
            return shortcut
        }
        else{
            return nil
        }
    }
    
    func setShortcut(_ value: String) -> Void{
        let prefs = UserDefaults.standard
        
        prefs.setValue(value, forKey: "shortcut")
        prefs.synchronize()
    }
    
    func getAPIURL() -> String?
    {
        return "http://38.100.119.14:8080/tazty-api"
    }
    
    func getAPPID() -> String?
    {
        return "fb93ceb9-17fc-11e6-a5a4-b661bcac687e"
    }
    
    func getContentType() -> String?
    {
        return "application/json"
    }
    
    func getOrangeTazty() -> UIColor?
    {
        return  UIColor.init(red: 245/255, green: 109/255, blue: 68/255, alpha: 1)
    }
    
    func getGrayTazty() -> UIColor?
    {
        return  UIColor.init(red: 127/255, green: 127/255, blue: 127/255, alpha: 1)
    }
}
