//
//  PublishVideoController.swift
//  Tazty
//
//  Created by Hugo Hernandez on 6/8/16.
//  Copyright © 2016 Tazty Inc. All rights reserved.
//

import UIKit
import Social
import Alamofire
import FBSDKShareKit
import Photos
import FBSDKLoginKit

class PublishVideoController: UIViewController, FBSDKSharingDelegate {
    @IBOutlet weak var imgThumbnail: UIImageView!
    @IBOutlet weak var txtRestauranteName: UITextField!
    @IBOutlet weak var txtDishname: UITextField!
    @IBOutlet weak var swFB: UISwitch!
    @IBOutlet weak var swTW: UISwitch!
    @IBOutlet weak var vwLoading: UIView!
    @IBOutlet weak var imgLoading: UIImageView!
    
    var urlVideo : URL?
    var foodName : String?
    var restaurantName : Restaurant?
    var thumb : UIImage?
    var saved = false
    var urlToShare : URL?
    var shareTWFB = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let edgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        edgePan.edges = .left
        
        self.imgThumbnail.image = thumb
        self.txtRestauranteName.text = restaurantName?.barname
        self.txtDishname.text = self.foodName
        
        let loadingGif = UIImage.gifWithName("load")
        self.imgLoading.image = loadingGif
        view.addGestureRecognizer(edgePan)
        
    }
    
    func shareFB()
    {
        if !self.saved
        {
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                // check if user authorized access photos for your app
                if authorizationStatus == .authorized {
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.urlVideo!)
                    }) { (sucess, error) in
                        if sucess
                        {
                            let video : FBSDKShareVideo = FBSDKShareVideo()
                            video.videoURL = self.urlToShare!
                            
                            let content : FBSDKShareVideoContent = FBSDKShareVideoContent()
                            content.video = video
                            content.hashtag = FBSDKHashtag.init(string: "#taztyapp")
                            
                            let dialog : FBSDKShareDialog = FBSDKShareDialog()
                            dialog.fromViewController = self
                            dialog.shareContent = content
                            dialog.mode = .shareSheet
                            if !dialog.canShow()
                            {
                                dialog.mode = .shareSheet
                            }
                            dialog.delegate = self
                            DispatchQueue.main.async {
                                dialog.show()
                            }
                        }
                        else
                        {
                            print("Error writing image with metadata to Photo Library: \(String(describing: error))")
                        }
                    }
                    
                    
                }
                
            })
            
        }
        else
        {
            let video : FBSDKShareVideo = FBSDKShareVideo()
            video.videoURL = self.urlToShare!
            //                video.previewPhoto = FBSDKSharePhoto.init(image: self.thumb, userGenerated: true)
            
            let content : FBSDKShareVideoContent = FBSDKShareVideoContent()
            content.video = video
            content.hashtag = FBSDKHashtag.init(string: "#taztyapp")
            
            let dialog : FBSDKShareDialog = FBSDKShareDialog()
            dialog.fromViewController = self
            dialog.shareContent = content
            dialog.mode = .shareSheet
            if !dialog.canShow()
            {
                dialog.mode = .shareSheet
            }
            dialog.delegate = self
            DispatchQueue.main.async {
               dialog.show()
            }
            
        }
        
    }
    
    func shareTW()
    {
        let urlShare = "http://38.100.119.14/fballr/uploads/" + (urlVideo?.lastPathComponent)!
        
        let tinyURL = "http://tinyurl.com/api-create.php?url=" + urlShare
        
        do
        {
            
            let shortURL = try String(contentsOf: URL(string: tinyURL)!)
            
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
                let twitterSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                twitterSheet.setInitialText(String(format: "%@ \n I just uploaded a new dish video on Tazty! Download free: www.taztyapp.com", shortURL))
                twitterSheet.completionHandler = {
                    (result : SLComposeViewControllerResult) in
                    DispatchQueue.main.async(execute: {
                        self.vwLoading.isHidden = true
                        self.performSegue(withIdentifier: "segue_to_home", sender: nil)
                    })
                }
                self.present(twitterSheet, animated: true, completion: nil)
            } else {
                self.vwLoading.isHidden = true
                self.performSegue(withIdentifier: "segue_to_home", sender: nil)
            }
            
        }
        catch(_)
        {
            self.vwLoading.isHidden = true
            self.performSegue(withIdentifier: "segue_to_home", sender: nil)
        }
    }
    
    func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        if recognizer.state == .recognized {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func changeFBSwitch(_ sender: AnyObject) {
    }
    
    @IBAction func actionPublish(_ sender: AnyObject) {
        print(self.urlVideo!.lastPathComponent)
        self.vwLoading.isHidden = false
        
        let finalName = String.init(format:"movie_%@.mp4", String(Date().timeIntervalSince1970)
            .replacingOccurrences(of: ".", with: ""))
        
        let imageData = UIImageJPEGRepresentation(self.thumb!, 1)
        let videoData = try? Data.init(contentsOf: self.urlVideo!)
        
        upload(multipartFormData: { (multipartFormdata) in
            multipartFormdata.append(imageData!, withName: "thumb", fileName: ("thumb_" + finalName + ".jpg"), mimeType: "image/jpeg")
            multipartFormdata.append(videoData!, withName: "video", fileName: finalName, mimeType: "video/mp4")
            multipartFormdata.append(Session.sharedInstance.getUserID()!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "user_id")
            multipartFormdata.append((self.restaurantName?.barname!.data(using: String.Encoding.utf8, allowLossyConversion: false)!)!, withName: "barname")
            multipartFormdata.append((self.restaurantName?.videos!.data(using: String.Encoding.utf8, allowLossyConversion: false)!)!, withName: "baraddress")
            multipartFormdata.append(self.foodName!.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: "foodname")
            multipartFormdata.append((self.restaurantName?.latitude!.data(using: String.Encoding.utf8, allowLossyConversion: false)!)!, withName: "latitude")
            multipartFormdata.append((self.restaurantName?.longitude!.data(using: String.Encoding.utf8, allowLossyConversion: false)!)!, withName: "longitude")
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: (Session.sharedInstance.getAPIURL()! + "/user/upload_post"), method: HTTPMethod.post, headers: ["T-APP-ID" : Session.sharedInstance.getAPPID()!]) { (encodingResult) in
            switch encodingResult{
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { response in
                    DispatchQueue.main.async(execute: {
                        let alert = UIAlertController(title: "Tazty", message: "Save video to your Camera Roll?", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                            self.share()
                        }))
                        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
                            PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                                // check if user authorized access photos for your app
                                if authorizationStatus == .authorized {
                                    var videoAssetPlaceholder : PHObjectPlaceholder!
                                    PHPhotoLibrary.shared().performChanges({
                                        let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.urlVideo!)
                                        videoAssetPlaceholder = request!.placeholderForCreatedAsset
                                    }) { (sucess, error) in
                                        if sucess
                                        {
                                            let localID = videoAssetPlaceholder.localIdentifier
                                            let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: .regularExpression, range: nil)
                                            let ext = "mp4"
                                            let assetURLStr = "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                                            self.saved = true
                                            self.urlToShare = URL(string:assetURLStr)
                                            self.share()
                                        }
                                        else
                                        {
                                            self.saved = false
                                            self.share()
                                        }
                                    }
                                }
                            })
                        }))
                        self.present(alert, animated: true, completion: nil)
                    })
                })
            case .failure(let encodingError):
                print(encodingError)
            }
        }
    }
    
    func share()
        
    {
        if !self.swFB.isOn && !self.swTW.isOn
        {
            self.vwLoading.isHidden = true
            self.performSegue(withIdentifier: "segue_to_home", sender: nil)
        }
        else if self.swFB.isOn && self.swTW.isOn
        {
            self.shareTWFB = true
            if !self.saved
            {
                let alert = UIAlertController(title: "Tazty", message: "If you don't save to camera roll, video will not be able to be shared on Facebook. ", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.shareTW()
                }))
                alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
                    
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        // check if user authorized access photos for your app
                        if authorizationStatus == .authorized {
                            var videoAssetPlaceholder : PHObjectPlaceholder!
                            PHPhotoLibrary.shared().performChanges({
                                let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.urlVideo!)
                                videoAssetPlaceholder = request!.placeholderForCreatedAsset
                            }) { (sucess, error) in
                                if sucess
                                {
                                    let localID = videoAssetPlaceholder.localIdentifier
                                    let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: .regularExpression, range: nil)
                                    let ext = "mp4"
                                    let assetURLStr = "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                                   // print(assetURLStr)
                                    self.saved = true
                                    self.urlToShare = URL(string:assetURLStr)
                                    self.shareFB()
                                }
                                else
                                {
                                    self.saved = false
                                }
                            }
                        }
                    })
                }))
                
                self.present(alert, animated: true, completion:nil)
            }
            else
            {
                self.shareFB()
            }
            
        }
        else if self.swFB.isOn
        {
            if !self.saved
            {
                let alert = UIAlertController(title: "Tazty", message: "If you don't save to camera roll, video will not be able to be shared on Facebook. ", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: { (action) -> Void in
                    self.vwLoading.isHidden = true
                    self.performSegue(withIdentifier: "segue_to_home", sender: nil)
                }))
                alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) -> Void in
                    PHPhotoLibrary.requestAuthorization({ (authorizationStatus: PHAuthorizationStatus) -> Void in
                        // check if user authorized access photos for your app
                        if authorizationStatus == .authorized {
                            var videoAssetPlaceholder : PHObjectPlaceholder!
                            PHPhotoLibrary.shared().performChanges({
                                let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: self.urlVideo!)
                                videoAssetPlaceholder = request!.placeholderForCreatedAsset
                            }) { (sucess, error) in
                                if sucess
                                {
                                    let localID = videoAssetPlaceholder.localIdentifier
                                    let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: .regularExpression, range: nil)
                                    let ext = "mp4"
                                    let assetURLStr = "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                                   // print(assetURLStr)
                                    self.saved = true
                                    self.urlToShare = URL(string:assetURLStr)
                                    self.shareFB()
                                }
                                else
                                {
                                    self.saved = false
                                }
                            }
                        }
                    })
                }))
                
                self.present(alert, animated: true, completion:nil)
            }
            else
            {
                self.shareFB()
            }
        }
        else if self.swTW.isOn
        {
            self.shareTW()
        }
    }
    
    
    func sharer(_ sharer: FBSDKSharing!, didCompleteWithResults results: [AnyHashable: Any]) {
        print(results)
        
        if self.shareTWFB
        {
            self.shareTW()
        }
        else
        {
            self.vwLoading.isHidden = true
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "segue_to_home", sender: nil)
            })
            
        }
    }
    
    func sharerDidCancel(_ sharer: FBSDKSharing!) {
        self.vwLoading.isHidden = true
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: "segue_to_home", sender: nil)
        })
    }
    
    public func sharer(_ sharer: FBSDKSharing!, didFailWithError error: Error!) {
        print(error)
        print(error.localizedDescription)
        self.vwLoading.isHidden = true
        DispatchQueue.main.async(execute: {
            self.performSegue(withIdentifier: "segue_to_home", sender: nil)
        })
    }
    
    
    @IBOutlet weak var onChangeFBSwitch: UISwitch!
    
    @IBAction func onBack(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segue_to_home"
        {
            let vc = segue.destination as! HomeController
            vc.isAnonymous = false
            vc.isFirstLoad = true
            vc.isInitialized = false
        }
    }
}
